var dialog = function (settings) {
    var defaults = {
        'type': 'info',
        'size': 'wide',
        'title': '',
        'url': ''
    };
    if ($.isPlainObject(settings)) {
        settings = $.extend(defaults, settings || {});
    } else {
        defaults['url']=settings;
        settings=defaults;
    }
    BootstrapDialog.show({
        id: 'modal_ajax',
        type: 'type-' + settings.type,
        size: 'size-' + settings.size,
        title: settings.title,
        message: $('<div></div>').load(settings.url, function (response, status, xhr) {
            if (xhr.status == 404) {
                $(this).html('<div class="wrap">' + '<h1>404 - File Not Found</h1>' + '<p>Controller or page is not found. </p>' + '<p>当前模块或页面未找到，请联系管理员。</p>' + '</div>');
            } else if (xhr.status == 500) {
                $(this).load('<div class="wrap">' + '<h1>500 - Have an Error</h1>' + '<p> </p>' + '<p>当前服务器发生错误，请联系管理员。</p>' + '</div>');
            }
        })
    });
};
dialog.close = function () {
    $('#modal_ajax').modal('hide');
};

var confirm=function(settings) {
    var defaults = {
        'title': '警告',
        'type': BootstrapDialog.TYPE_WARNING,
        'message': '',
        'url':"",
        'data':"",
        'ajax':'get', //true or false or post
        'cancel':'取消',
        'ok': '确认',
        'action':'',
        'success':function(){},
    };
    if ($.isPlainObject(settings)) {
        settings = $.extend(defaults, settings || {});
    } else {
        defaults['message']=settings;
        settings=defaults;
    }
    BootstrapDialog.confirm({
        title: settings.title,
        message: settings.message,
        type: settings.type,
        btnCancelLabel: settings.cancel,
        btnOKLabel: settings.ok,
        callback: function (result) {
            if (result) {
                if ($.isFunction(settings.action) ) {
                    settings.action();
                } else if (settings.url) {
                    if (settings.ajax) {
                        $.ajax({
                            'url': settings.url,
                            'type': settings.ajax.toLowerCase()==="post"?"post":"get",
                            'data': settings.data,
                            success:function(data) {
                                showMessage(data.message);
                                if (data.status!="Error") {
                                    settings.success();
                                }
                            }
                        })
                    } else {
                        windows.locale.href=settings.url;
                    }
                }
            }
        }
    });
}
var showMessage = function (options) {
    var settings = {
        icon: 'fa fa-info-circle',
        title: '',
        message: '',
        type: 'info',
        delay: 3000
    };
    if (typeof options === "string") {
        settings.message = options;
    } else {
        settings = $.extend(settings, options || {});
    }
    $.notify({
        title: settings.title,
        message: settings.message
    }, {
        type: 'pastel-' + settings.type,
        delay: settings.delay,
        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">'
        + '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>'
        + '<span data-notify="title">{1}</span>'
        + '<span data-notify="message">{2}</span>' + '</div>'
    });
};
var cover = function (close) {
    if (close) {
        $('#cover').remove();
        $('#loading').remove();
    } else {
        $('body').append(
            $('<div>', {'id': 'cover'}).css({
                'position': 'absolute',
                'left': '0px',
                'top': '0px',
                'background': 'rgba(0, 0, 0, 0.4)',
                'width': '100%',
                'height': '100%',
                'filter': 'alpha(opacity=60)',
                'opacity': '0.6',
                'display': 'block',
                'z-Index': '1030'
            })
        ).append(
            $('<div>', {'id': 'loading'}).css({
                'position': 'absolute',
                'width': '500px',
                'height': '30px',
                'top': '40%',
                'left': '40%',
                'color': '#fff',
                'display': 'block',
                'cursor': 'pointer',
                'z-Index': '1031'
            }).html("<i class='icon-spin4 animate-spin'></i> 正在加载试卷，请稍等...")
        )
    }
};

var store = function () {
    if (typeof(arguments[0]) == 'object') {
        arguments[0].each(function (name, val) {
            localStorage.setItem(name, val);
        })
    } else if (arguments[1]) {
        localStorage.setItem(arguments[0], arguments[1]);
    } else {
        con
        return localStorage.getItem(arguments[0])?localStorage.getItem(arguments[0]):null;
    }
};

function jump(url){
    var Cwin= window.open(url,'bfs','fullscreen,scrollbars');

}


function fullScreen() {
    var docElm = document.documentElement;
    //W3C
    if (docElm.requestFullscreen) {
        docElm.requestFullscreen();
    }
    //FireFox
    else if (docElm.mozRequestFullScreen) {
        document.documentElement.mozRequestFullScreen();
    }
    //Chrome等
    else if (docElm.webkitRequestFullScreen) {
        docElm.webkitRequestFullScreen();
    }
    //IE11
    else if (elem.msRequestFullscreen) {
        elem.msRequestFullscreen();
    }
}

function exitFullScreen() {
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
    } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if (document.oRequestFullscreen) {
        document.oCancelFullScreen();
    } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    }
}




