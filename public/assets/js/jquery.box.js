/**
 * Created by joe on 17-10-25.
 */
(function ($) {
    var settings = {
        speed: 500,
        collapseIcon: 'fa-chevron-down',
        expandIcon: 'fa-chevron-up',
        removeIcon: 'fa-times',
        refreshIcon: 'fa-refresh',
        minimizeIcon: 'fa-window-minimize',
        maximizeIcon: 'fa-window-maximize',
        listenBody: true,
        refresh: {
            url: '',
            type: 'get',
            data: {},
            async: true,
            dataType: '',
            beforeSend: function () {
            },
            error: function () {
            }
        }
    };

    var collapse = function (obj) {
        obj.addClass('collapsed')
            .find('.box-tools')
            .find('.' + settings.collapseIcon)
            .removeClass(settings.collapseIcon)
            .addClass(settings.expandIcon);
        obj.children('.box-body,.box-footer').slideUp(settings.speed);
    };

    var expand = function (obj) {
        obj.removeClass('collapsed')
            .find('.box-tools')
            .find('.' + settings.expandIcon)
            .removeClass(settings.expandIcon)
            .addClass(settings.collapseIcon);
        obj.find('.box-body,.box-footer').slideDown(settings.speed);
    };

    var remove = function (obj) {
        obj.slideUp(settings.speed, function () {
            obj.remove();
        })
    };

    var maximize = function (obj) {
        if (obj.is('.collapsed')) {
            expand(obj);
            obj.data('collapsed', true);
        }
        console.log(obj.height())
        obj.css({
            'position': 'fixed',
            'top': 0,
            'left': 0,
            'z-index': 9999,
            'width': '100%'
        }).height('100%')
            .data('maximize', true)
            .find('.box-tools')
            .find('.' + settings.maximizeIcon)
            .removeClass(settings.maximizeIcon)
            .addClass(settings.minimizeIcon);
    }

    var minimize = function (obj) {
        if (obj.data('collapsed')) collapse(obj);
        obj.css({
            'position': 'static',
            'z-index': 2
        }).width('auto')
            .height('auto')
            .data('maximize', false)
            .find('.box-tools')
            .find('.' + settings.minimizeIcon)
            .removeClass(settings.minimizeIcon)
            .addClass(settings.maximizeIcon);
    }

    var toggleImize = function (obj) {
        obj.data('maximize') ? minimize(obj) : maximize(obj);
    }

    var toggle = function (obj) {
        obj.is('.collapsed') ? expand(obj) : collapse(obj);
    }

    var refresh = function (obj) {
        var refreshOption = settings.refresh;
        if (typeof obj.data('refresh') == 'object') {
            refreshOption = $.extend({}, refreshOption, obj.data('refresh'));
        } else if (typeof obj.data('refresh') == 'string') {
            refreshOption.url = obj.data('refresh');
        }

        refreshOption.success = function (result) {
            obj.find('.box-body').html(result);
            obj.find('.overlay').remove();
        }

        obj.append($('<div>', {'class': 'overlay'}));

        $.ajax(refreshOption);
    }

    var addBtn = function (obj) {
        if (obj.children('.box-header').length < 1) return false;
        var btn = (obj.find('.box-tools').length < 1) ? $('<div>', {'class': 'box-tools pull-right'}) : obj.children('.box-header').children('.box-tools');
        var collapseBtn = $('<button>', {
            'class': 'btn btn-box-tool',
            'data-box': 'collapse'
        }).append($('<i>', {'class': 'fa ' + settings.collapseIcon}));
        var removeBtn = $('<button>', {
            'class': 'btn btn-box-tool',
            'data-box': 'remove'
        }).append($('<i>', {'class': 'fa ' + settings.removeIcon}));
        var refreshBtn = $('<button>', {
            'class': 'btn btn-box-tool',
            'data-box': 'refresh'
        }).append($('<i>', {'class': 'fa ' + settings.refreshIcon}));
        var maximizeBtn = $('<button>', {
            'class': 'btn btn-box-tool',
            'data-box': 'maximize'
        }).append($('<i>', {'class': 'fa ' + settings.maximizeIcon}));

        if (obj.hasClass('box-refresh') && btn.find('[data-box="refresh"]').length < 1) {
            btn.append(refreshBtn);
        }
        if (obj.hasClass('box-collapse') && btn.find('[data-box="collapse"]').length < 1) {
            btn.append(collapseBtn);
        }
        if (obj.hasClass('box-maximize') && btn.find('[data-box="maximize"]').length < 1) {
            btn.append(maximizeBtn);
        }
        if (obj.hasClass('box-remove') && btn.find('[data-box="remove"]').length < 1) {
            btn.append(removeBtn);
        }

        obj.children('.box-header').append(btn);
    }

    var listen = function (obj) {
        $(obj).on('click', '[data-box="collapse"]', function () {
            toggle($(this).parents('.box:first'));
        });
        $(obj).on('click', '[data-box="remove"]', function () {
            remove($(this).parents('.box:first'));
        });
        $(obj).on('click', '[data-box="refresh"]', function () {
            refresh($(this).parents('.box:first'));
        });
        $(obj).on('click', '[data-box="maximize"]', function () {
            toggleImize($(this).parents('.box:first'));
        });
    }

    var methods = {
        init: function (options) {
            settings = $.extend({}, settings, options);
            if (settings.listenBody) listen('body');
            return this.each(function () {
                var $this = $(this);
                addBtn($this);
                if ($this.is('.collapsed')) {
                    collapse($this);
                }
                if ($this.is('.box-refresh')) {
                    refresh($this);
                }
                if (settings.listenBody == false) listen($this);
            });
        },
        toggle: function (obj) {
            toggle(obj)
        },
        expand: function (obj) {
            expand(obj)
        },
        collapse: function (obj) {
            collapse(obj)
        },
        remove: function (obj) {
            remove(obj)
        },
        refresh: function (obj) {
            refresh($(this))
        },
        window: function (obj) {
            toggleImize(obj)
        },
        maximize: function (obj) {
            maximize(obj)
        },
        minimize: function (obj) {
            minimize(obj)
        },
        addBtn: function () {
            return this.each(function () {
                addBtn($(this));
            })
        }
    };

    $.fn.box = function () {
        var method = arguments[0];

        if (methods[method]) {
            method = methods[method];
            arguments = Array.prototype.slice.call(arguments, 1);
        } else if (typeof(method) == 'object' || !method) {
            method = methods.init;
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.box');
            return this;
        }

        return method.apply(this, arguments);
    }

})(jQuery);

$(function () {
    $('.box').box();
})
