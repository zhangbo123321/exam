<?php namespace App\Controllers;


use App\Models\AnnounceModel;
use App\Models\ZoneModel;


class Front extends Application {

    public function __construct(...$params) {
        parent::__construct(...$params);
        if (!$this->user) {
            $this->showError("用户尚未登录，请登录后重新打开。", site_url('/'));
        }
    }

    public function index() {
        $zoneModel = new ZoneModel();
        $zone = $zoneModel->getParents($this->user['zone'], 'id');
        $organizeModel = new \App\Models\OrganizeModel();
        $organize = $organizeModel->findUserOrganize($this->user['id'], 'id');
        //获取可参加的考场
        $roomModel = new \App\Models\RoomModel();
        $this->data['room'] = $roomModel->findUserRoom($zone, $organize);
        $this->data['exercise'] = $roomModel->findUserRoom($zone, $organize, "*", 1);
        //获取历史记录
        //$testModel = new \App\Models\TestModel();
        //$this->data['test'] = $testModel->findUserTest($this->user['id']);

        $announce = new AnnounceModel();
        $this->data['announces'] = $announce->orderBy('date', 'DESC')
                                            ->limit(5)
                                            ->find();

        $this->render("front/home", "概览");
    }

}
