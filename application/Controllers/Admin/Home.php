<?php namespace App\Controllers\Admin;

use \App\Controllers\Application;

class Home extends Application {

	public function __construct(...$params) {
		parent::__construct(...$params);
		if (!$this->user) {
            $this->showError("用户尚未登录，请登录后重新打开。", site_url(''));
		}
	}

	public function index() {
		$this->data['users'] = $this->db->table('users')->where('deleted', 0)->countAllResults();
		$this->data['pages'] = $this->db->table('page')->countAllResults();
		$this->data['questions'] = $this->db->table('question')->countAllResults();
		$this->data['rooms'] = $this->db->table('room')->countAllResults();
		$this->data['desc'] = "概览网站内容";
		$this->render("admin/home", "概览");
	}

}
