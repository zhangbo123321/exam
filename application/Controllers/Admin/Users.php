<?php namespace App\Controllers\Admin;

use \App\Controllers\Application;
use \App\Libraries\Datatable;
use \App\Models\GroupModel;
use \App\Models\ZoneModel;

class Users extends Application {

    public function __construct(...$params) {
        parent::__construct(...$params);

        if (!$this->user) {
            $this->showError("用户尚未登录，请登录后重新打开。", site_url(''));
        }
    }

    public function index() {
        $this->render('admin/users/list', '用户管理');
    }

    public function table($deleted = 0) {
        $table = $this->request->getGet();
        $users = new Datatable('users');

        if (isset($table['search']) && $table['search'] != "") {
            $users->like('users.name', $table['search']);
        }

        if (isset($table['sort'])) {
            $users->orderBy($table['sort'], $table['order']);
        }

        if (isset($table['limit'])) {
            $users->limit($table['limit'], $table['offset']);
        }

        $usersList = $users->select('users.*,groups.name as groupName,zone.name as zoneName')
                           ->join('groups', 'users.group=groups.id', 'left')
                           ->join('zone', 'users.zone=zone.id', 'left')
                           ->where('deleted', $deleted)
                           ->json();

        echo $usersList;
    }

    public function add($id = '') {
        if ($id) {
            $this->data['editUser'] = $this->userModel->find($id);
        }
        $group = new GroupModel();
        $this->data['groups'] = $group->findAll();
        $cities = new ZoneModel();
        $this->data['zones'] = $cities->findAll();
        $this->render('admin/users/add');
    }

    public function save() {
        $id=$this->request->getPost('id')?:0;
        $password=$this->request->getPost('password');
        $user=[
            "id" => $id,
            "username" => $this->request->getPost('username'),
            "email" => $this->request->getPost('email')?:null,
            "group" => $this->request->getPost('group_id'),
            "zone" => $this->request->getPost('zone_id')
        ];
        if ($id) {
            if ($password) $user['password']=md5($password);
        } else {
            $user['password']=$password?md5($password):md5('123');
        }

        $userModel=new \App\Models\UserModel();
        $userModel->save($user);
        $this->showJson('保存用户成功','Success');
    }

    public function delete($recover = false) {
        $id = $this->request->getPostGet('id');

        if (!$id) {
            $this->showJson('没有选择要删除的用户', 'Error');
        }

        if (!is_array($id)) {
            $ids[] = $id;
        } else {
            $ids = $id;
        }

        $deleted = $recover == 'true' ? 0 : 1;
        $deleted_message = $recover == 'true' ? '恢复成功' : '删除成功';
        $this->db->table('users')
                 ->whereIn('id', $ids)
                 ->set('deleted', $deleted)
                 ->update();

        $this->showJson($deleted_message, 'Success');
    }

}
