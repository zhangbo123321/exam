<?php

namespace App\Controllers\Admin;

use \App\Controllers\Application;
use App\Models\CategoryModel;


class Category extends Application {

    public function __construct(...$params) {
        parent::__construct(...$params);
        if (!$this->user) {
            $this->response->redirect('/');
        }
    }

    public function index() {
        $zoneModel = new CategoryModel();
        $this->data['categories'] = json_encode($zoneModel->tree());
        $this->data['desc'] = "类别管理，可以使用拖动";
        $this->render('admin/category/list', '类别管理');
    }

    public function save() {
        $name = $this->request->getPost('name');
        $parent = $this->request->getPost('parent') ?: 0;

        $city = ['id' => 0, 'name' => $name, 'parent' => $parent];

        $zoneModel = new CategoryModel();
        $zoneModel->insert($city);
        $city['id'] = $this->db->insertID();
        $this->showJson($city);
    }

    public function parent() {
        $id = $this->request->getPost('id');
        $parent = $this->request->getPost('parent');

        $zoneModel = new CategoryModel();
        $zoneModel->update($id, ['parent' => $parent]);
    }

    public function name() {
        $id = $this->request->getPost('id');
        $name = $this->request->getPost('name');

        $zoneModel = new CategoryModel();
        $zoneModel->update($id, ['name' => $name]);
    }

    public function delete() {

    }

}
