<?php namespace App\Controllers\Admin;

use \App\Controllers\Application;
use \App\Models\ZoneModel;

class Zone extends Application {

	public function __construct(...$params) {
		parent::__construct(...$params);
		if (!$this->user) {
            $this->response->redirect('/');
		}
	}

	public function index() {
		$zoneModel = new ZoneModel();
		$this->data['cities'] = json_encode($zoneModel->tree());
        $this->data['desc'] = "管理区域，可以使用拖动";
		$this->render('admin/zone/list', '区域管理');
	}

	public function save() {
		$name = $this->request->getPost('name');
		$parent = $this->request->getPost('parent') ?: 0;

		$city = ['id' => 0, 'name' => $name, 'parent' => $parent];

		$zoneModel = new ZoneModel();
		$zoneModel->insert($city);
		$city['id'] = $this->db->insertID();
		$this->showJson($city);
	}

	public function parent() {
		$id = $this->request->getPost('id');
		$parent = $this->request->getPost('parent');

		$zoneModel = new ZoneModel();
		$zoneModel->update($id, ['parent' => $parent]);
	}

	public function name() {
		$id = $this->request->getPost('id');
		$name = $this->request->getPost('name');

		$zoneModel = new ZoneModel();
		$zoneModel->update($id, ['name' => $name]);
	}

}
