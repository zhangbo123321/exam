<?php namespace App\Controllers;

use App\Models\AnnounceModel;

class Home extends Application {

    public function __construct(...$params) {
        parent::__construct(...$params);
    }

    public function index() {
        if ($this->user) {
            switch ($this->user['group']['level']) {
                case 9:
                    $this->response->redirect("admin");
                    break;
                case 7:
                    $this->response->redirect("teacher");
                    break;
                case 1:
                    $this->response->redirect('front');
                    break;
                default:
                    $this->render('/front/user/login', '用户登录', 'front/clean');
                    return;
            }
        } else {
            $showAnnounce=$this->setting->getSetting('homeShowAnnouncement');
            if ($showAnnounce=='true') {
                $announce=new AnnounceModel();
                $this->data['announce'] = $announce->orderBy('date','DESC')->first();
            }
            $this->render('/front/user/login', '用户登录', 'front/clean');
        }
    }

}
