<?php namespace App\Controllers;

class User extends Application {

	public function __construct(...$params) {
        parent::__construct(...$params);
	}

	public function index() {
		if (!$this->user) {
			$this->data['pageTitle'] = '用户登录';
			$this->render('front/user/login', 'login_page', "");
		} else {
            $this->response->redirect(site_url(''));
		}
	}

	public function login() {
		$email = $this->request->getPost('username');
		$password = $this->request->getPost('password');

		$loginLog = $this->userModel->doLogin($email, $password);
		$this->showJson($loginLog);
	}

	public function logout() {
		$this->session->destroy();
		helper('cookie');
		set_cookie('dmUrl', site_url('/'));
        $this->response->redirect(site_url(''));
	}

	public function register() {
        $zoneModel = new \App\Models\ZoneModel();
        $this->data['cities'] = json_encode($zoneModel->getChildren(0));
	    $this->render('front/user/register','用户注册','front/clean');
    }

	public function profile($id = '') {
		if ($id) {
			$user = $this->userModel->find($id);
		} else {
			$user = $this->session->get('user');
		}

		$this->data['cuser'] = $user;

		$this->render('front/user/profile');
	}

	public function password() {
		$this->render("front/user/password", "修改密码");
	}
}
