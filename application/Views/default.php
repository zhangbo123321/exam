<!DOCTYPE html >
<html>
<head>
    <meta charset="utf-8"/>
    <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
    <meta content="" name="description"/>
    <meta content="" name="keywords"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
    <title>
        <?= $title ?> - <?= $homeTitle ?>
    </title>
    <link href="<?= $assets ?>/css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css"/>
    <link href="<?= $assets ?>/css/icons.css" media="screen" rel="stylesheet" type="text/css"/>
    <link href="<?= $assets ?>/css/AdminLTE.min.css" media="screen" rel="stylesheet" type="text/css"/>
    <link href="<?= $assets ?>/css/front.css" media="screen" rel="stylesheet" type="text/css"/>
    <link href="<?= $assets ?>/css/_all-skins.min.css" media="screen" rel="stylesheet" type="text/css"/>
    <link href="<?= $assets ?>/css/skin-blue.min.css" media="screen" rel="stylesheet" type="text/css"/>
    <link href="<?= $assets ?>/css/bootstrap-dialog.min.css" media="screen" rel="stylesheet" type="text/css"/>
    <link href="<?= $assets ?>/css/nprogress.css" media="screen" rel="stylesheet" type="text/css"/>
    <script src="<?= $assets ?>/js/jquery.min.js" type="text/javascript"></script>
    <script src="<?= $assets ?>/js/bootstrap.min.js" type="text/javascript"></script>
</head>
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">
    <header class="main-header">
        <nav class="navbar navbar-static-top navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?= site_url('/') ?>" data-pjax>
                        <b>EW</b>Exam
                    </a>
                    <button class="navbar-toggle collapsed" data-target="#navbar-collapse" data-toggle="collapse" type="button">
                        <i class="fa fa-bars">
                        </i>
                    </button>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li>
                            <a data-pjax="" href="<?= site_url('/') ?>">
                                <i class="fa fa-home"></i>
                                首页
                                <span class="sr-only">
											(current)
										</span>
                            </a>
                        </li>
                        <?php if ($user['group']['level'] == 9): ?>
                            <li>
                                <a aria-expanded="false" class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-magic"></i>
                                    考试管理
                                    <span class="caret">
										</span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-group">
                                            </i>
                                            学员分组
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-th-large">
                                            </i>
                                            考场设定
                                        </a>
                                    </li>
                                    <li class="divider">
                                    </li>
                                    <li>
                                        <a href="<?= site_url('admin/question') ?>">
                                            <i class="fa fa-question-circle-o">
                                            </i>
                                            试题管理
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon icon-note">
                                            </i>
                                            试卷管理
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-cogs">
                                    </i>
                                    系统管理
                                    <span class="caret">
										</span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="<?= site_url('admin/zone') ?>" data-pjax>
                                            <i class="fa fa-sitemap"></i>
                                            区域管理
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('admin/users') ?>" data-pjax="true">
                                            <i class="fa fa-user-o"></i>
                                            用户管理
                                        </a>
                                    </li>
                                    <li class="divider">
                                    </li>
                                    <li>
                                        <a href="<?= site_url('admin/category') ?>" data-pjax="true">
                                            <i class="fa fa-cubes"></i>
                                            类别管理
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-th-list"></i>
                                            题型管理
                                        </a>
                                    </li>
                                    <li class="divider">
                                    </li>
                                    <li>
                                        <a href="<?= site_url('admin/setting') ?>" data-pjax>
                                            <i class="fa fa-cog"></i>
                                            参数设置
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a data-pjax="" href="<?= site_url('/admin/announce') ?>">
                                    <i class="fa fa-bullhorn"></i>
                                    公告管理
                                </a>
                            </li>
                        <?php else: ?>
                            <li>
                                <a data-pjax="" href="<?= site_url('test/records') ?>">
                                    <i class="fa fa-tachometer"></i>
                                    考试记录
                                </a>
                            </li>
                        <?php endif; ?>

                    </ul>
                </div>

                <!-- /.navbar-collapse -->
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- Notifications Menu -->
                        <li class="dropdown notifications-menu">
                            <!-- Menu toggle button -->
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-bell-o">
                                </i>
                                <span class="label label-warning">
											10
										</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">
                                    You have 10 notifications
                                </li>
                                <li>
                                    <!-- Inner Menu: contains the notifications -->
                                    <ul class="menu">
                                        <li>
                                            <!-- start notification -->
                                            <a href="#">
                                                <i class="fa fa-users text-aqua">
                                                </i>
                                                5 new members joined today
                                            </a>
                                        </li>
                                        <!-- end notification -->
                                    </ul>
                                </li>
                                <li class="footer">
                                    <a href="#">
                                        View all
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- Tasks Menu -->
                        <!-- User Account Menu -->
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a aria-expanded="false" class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <!-- The user image in the navbar-->
                                <img alt="User Image" class="user-image" src="<?= $assets ?>/images/avatar/<?= $user['avatar'] ?: 'default.jpg' ?>">
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs">
											<?= $user['username'] ?>
										</span>
                                </img>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                    <img alt="User Image" class="img-circle" src="<?= $assets ?>/images/avatar/<?= $user['avatar'] ?: 'default.jpg' ?>">
                                    <p>
                                        <?= $user['username'] ?>
                                        <small>
                                            <?= $user['group']['name'] ?>
                                        </small>
                                    </p>
                                    </img>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a class="btn btn-default btn-flat" data-pjax href="<?= site_url('user/password') ?>">
                                            <i class="fa fa-key">
                                            </i>
                                            密码
                                        </a>
                                    </div>
                                    <div class="pull-right">
                                        <a class="btn btn-default btn-flat" href="<?= site_url('/user/logout') ?>">
                                            <i class="fa fa-power-off">
                                            </i>
                                            退出
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-custom-menu -->
            </div>
            <!-- /.container-fluid -->
        </nav>
    </header>
    <div class="content-wrapper">
        <div class="container">
            <section class="content-header">
                <h1>
                    <?= $title ?>
                    <?php if (isset($desc)): ?>
                        <small><?= $desc ?></small><?php endif; ?>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="<?= site_url('/') ?>"><i class="fa fa-home"></i> 首页</a></li>
                    <li class="active"><?= $title ?></li>
                </ol>
            </section>
            <div id="main-content">
                <?= $content ?>
            </div>
        </div>
    </div>
</div>

<script src="<?= $assets ?>/js/bootstrap-dialog.min.js" type="text/javascript"></script>
<script src="<?= $assets ?>/js/bootstrap-notify.min.js" type="text/javascript"></script>
<script src="<?= $assets ?>/js/jquery.pjax.js" type="text/javascript"></script>
<script src="<?= $assets ?>/js/jquery.box.js" type="text/javascript"></script>
<script src="<?= $assets ?>/js/nprogress.js" type="text/javascript"></script>
<script src="<?= $assets ?>/js/function.js" type="text/javascript"></script>
<script type="text/javascript">

    $(function () {
        $('body').on('click', '[data-dialog]', function () {
            var settings = {
                'title': $(this).attr('title') ? $(this).attr('title') : $(this).attr('data-original-title'),
                'url': $(this).data('dialog')
            };
            dialog(settings);
        });
        $(document).on("click", "[data-pjax]", function () {
            var url = $(this).attr("href");

            $.pjax({
                url: url,
                container: '#main-content'
            });
        });
        NProgress.configure({
            speed: 100
        });
        $(document).on('pjax:start', NProgress.start).on('pjax:end', NProgress.done);
    });
</script>
</body>
</html>