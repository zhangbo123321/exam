<div class="row">

    <div class="col-sm-9">
        <div class="box box-primary box-collapse box-maximize">
            <div class="box-header with-border">
                <h3 class="box-title">考场</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="authorBox">
                <?php foreach ($room as $r): ?>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h4><?= $r['name'] ?></h4>
                                <p>
                                    时间:<?= $r['time'] ?>
                                </p>
                            </div>

                            <a href="#" class="small-box-footer enterTest" data-id="<?=$r['id']?>">
                                参加 <i class="fa fa-arrow-circle-right"></i>
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <!-- /.box-body -->
        </div>
        <div class="box box-warning box-collapse box-maximize">
            <div class="box-header with-border">
                <h3 class="box-title">练习</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="authorBox">
                <?php foreach ($exercise as $r): ?>
                    <div class="col-lg-4  col-sm-6 col-xs-12">
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h4><?= $r['name'] ?></h4>
                                <p>
                                    时间:<?= $r['time'] ?>
                                </p>
                            </div>

                            <a href="#" class="small-box-footer enterTest" data-id="<?=$r['id']?>">
                                参加 <i class="fa fa-arrow-circle-right"></i>
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
    <div class="col-sm-3">
        <div class="panel box box-success box-remove">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <i class="fa fa-bullhorn"></i>
                    公告
                </h3>
            </div>

            <ul class="panel-group list-group announce" id="accordion" role="tablist" aria-multiselectable="true">
                <?php foreach ($announces as $announce): ?>
                    <li class="panel list-group-item">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#announce<?= $announce['id'] ?>" aria-expanded="false">
                                <?= $announce['title'] ?>
                            </a>
                        </h4>

                        <div id="announce<?= $announce['id'] ?>" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <?= $announce['content'] ?>
                                <p class="text-right"><?= $announce['date'] ?></p>
                            </div>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>

</div>


<script type="text/javascript">
    $(function () {
        $('.panel-group').find('.panel:first').find('a:first').attr("aria-expanded", true);
        $('.panel-group').find('.panel-collapse:first').addClass('in');

        $('.enterTest').click(function () {
            var id=$(this).data('id');

            confirm({
                message: "<h5 class=\"text-left\">\n" +
                    "                <ol>\n" +
                    "                    <li>一旦参加考试中途不能退出（按ESC键），退出将自动提交试卷。考试中切换屏幕、改变屏幕大小都将自动提交试卷。</li>\n" +
                    "                    <li>考试时只许携带考试必备的耳机、铅笔、橡皮、黑色字迹的签字笔、小刀等。书籍、笔记、电子词典等，均不准带入考场。考试时更不准携带手机等通讯工具进入考场，一经发现按作弊处理。</li>\n" +
                    "                    <li>考试时交头接耳、左顾右盼、大声喧哗、不服从监考教师安排者，将按照考试管理的有关规定，从严论处。</li>\n" +
                    "                </ol>\n" +
                    "            </h5>",
                'cancel':'退出考试',
                'ok': '进入考试',
                'action': function () {
                    jump('<?=site_url("test/")?>'+id);
                }
            })
        })
    })
</script>