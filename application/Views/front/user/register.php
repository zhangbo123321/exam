<div class="box box-primary" style="width:480px;margin: 0 auto;">
    <div class="box-header with-border">
        <h3 class="box-title">用户注册</h3>
    </div>
    <div class="box-body">
        <form>
            <div class="form-group">
                <label for="exampleInputEmail1">用户名</label>
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"></i> </span>
                    <input type="text" class="form-control" name="username" placeholder="Username">
                </div>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">密码</label>
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-key"></i> </span>
                    <input type="password" class="form-control" name="password" placeholder="Password">
                </div>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">重复密码</label>
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-key"></i> </span>
                    <input type="password" class="form-control" name="repassword" placeholder="Password">
                </div>
            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">选择地区</label>
                <select name="zone" class="form-control"></select>
            </div>

            <button class="btn btn-success btn-block" id="register" type="submit">
                注册
            </button>
            <a class="btn btn-default btn-block" href="<?=site_url('')?>" type="button">
                登录
            </a>
        </form>
    </div>
</div>

<link href="<?= $assets ?>/css/jquery.selectTree.css" media="screen" rel="stylesheet" type="text/css"/>
<script src="<?=$assets?>/js/jquery.selectTree.js"></script>

<script type="text/javascript">
    $(function() {
        $('[name=zone]').select({
            data:<?=$cities?>
        })
    })
</script>