<?php
$fm=new \App\Models\FollowModel();
foreach ($followers as $follower):
    $mutual=$fm->mutual($cuser['id'],$follower['id']);
    ?>
    <div class="media user-follower">
        <img src="<?= $assets . '/images/avatar/' . ($follower['avatar'] ?: 'default.jpg') ?>" height="40" class="img-circle pull-left">
        <div class="media-body">
            <a href="<?= site_url('user/profile/' . $follower['id']) ?>" data-addtab="userProfile_<?=$follower['id']?>" data-title="<?= $follower['name'] ?>的资料">
                <?= $follower['name'] ?> <br/>
                <?=$follower['about']?>
            </a>
        </div>
        <div class="btn-group btn-group-sm pull-right">
        <button class="btn  <?=$mutual?'btn-toggle-following':''?>" type="button">
            <i class="icon-heart-1"></i>
            关注
        </button>
        <button class="btn btn-info" type="button">
            <i class="icon-bell"></i>
            消息
        </button>
        </div>
    </div>
<?php endforeach; ?>
