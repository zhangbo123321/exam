<div class="user-profile">
    <div class="profile-header-background">
        <img src="<?= $assets ?>/images/city.jpg" alt="Profile Header Background"/>
    </div>
    <div class="row" style="max-width: 980px;margin:0 auto;">
        <div class="col-md-4">
            <div class="profile-info-left">
                <div class="text-center">
                    <img src="<?= $assets ?>/images/avatar/<?= isset($cuser['avatar']) ? $cuser['avatar'] : "default.jpg" ?>" class="avatar img-circle" alt="">
                    <h2><?= $cuser['name'] ?></h2>
                </div>
                <div class="action-buttons">
                    <div class="row">
                        <?php if ($cuser['id'] != $user['id']): ?>
                            <div class="col-xs-4">
                                <a href="#" class="btn btn-success btn-block"><i class="icon-heart-empty"></i> 关注</a>
                            </div>
                            <div class="col-xs-4">
                                <a href="#" class="btn btn-info btn-block" data-addtab="sendMail" data-url="<?= $user == $cuser ? 'mail' : 'mail/send' ?>"><i class="icon-mail"></i> 邮件</a>
                            </div>
                            <div class="col-xs-4">
                                <a href="#" class="btn btn-primary btn-block" data-addtab="sendMessage" data-url="<?= $user == $cuser ? 'message' : 'message/send' ?>"><i class="icon-bell"></i> 消息</a>
                            </div>
                        <?php else: ?>
                            <div class="col-xs-12">
                                <a href="#" class="btn btn-success btn-block"><i class="icon-edit"></i> 编辑个人资料</a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="section">
                    <h3>个人简介</h3>
                    <p><?= $cuser['about'] ?: '这家伙很懒，什么都没写。' ?></p>
                </div>

                <div class="section">
                    <h3>发布小说</h3>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <?php foreach ($stories as $story): ?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#story_<?= $cuser['id'] . '_' . $story['id'] ?>">
                                            <?= $story['name'] ?>
                                            <i class="icon-up-open-mini pull-right icon-expanded"></i>
                                            <i class="icon-down-open-mini pull-right icon-collapsed"></i>
                                        </a>
                                        <a class="btn btn-xs btn-link pull-right" href="<?=site_url("story/".$story['id'])?>" target="_blank">
                                            <i class="icon-eye" title="阅读"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="story_<?= $cuser['id'] . '_' . $story['id'] ?>" class="panel-collapse collapse">
                                    <div class="panel-body story-desc">
                                        <img src="<?= $assets ?>/images/covers/<?= $story['image'] ?>">
                                        <?= $story['desc'] ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="profile-info-right">
                <div class="row section">
                    <div class="col-sm-offset-3 col-sm-3 col-xs-4 text-center">
                        <div class="quick-info horizontal">
                            <i class="icon-book pull-left bg-seagreen"></i>
                            <p><?= $storyNum ?><span>小说</span></p>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-4 text-center">
                        <div class="quick-info horizontal">
                            <i class="icon-heart-1 pull-left bg-orange"></i>
                            <p><?= $followersNum ?><span>粉丝</span></p>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-4 text-center">
                        <div class="quick-info horizontal">
                            <i class="icon-thumbs-up pull-left bg-green"></i>
                            <p><?= $followingNum ?><span>关注</span></p>
                        </div>
                    </div>
                </div>
                <ul class="nav nav-pills nav-pills-custom-minimal custom-minimal-bottom" id="userProfileTabs<?= $cuser['id'] ?>">
                    <li class="active"><a href="#userNews_<?= $cuser['id'] ?>" data-toggle="tab">动态</a></li>
                    <li><a href="#userBooks_<?= $cuser['id'] ?>" data-toggle="tab">书架</a></li>
                    <li><a href="#userBlog_<?= $cuser['id'] ?>" data-toggle="tab">博客</a></li>
                    <li><a href="#userFollowers_<?= $cuser['id'] ?>" data-toggle="tab">粉丝</a></li>
                    <li><a href="#userFollowing_<?= $cuser['id'] ?>" data-toggle="tab">关注</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active" id="userNews_<?= $cuser['id'] ?>"></div>
                    <div role="tabpanel" class="tab-pane fade" id="userBooks_<?= $cuser['id'] ?>"></div>
                    <div role="tabpanel" class="tab-pane fade" id="userBlog_<?= $cuser['id'] ?>"></div>
                    <div role="tabpanel" class="tab-pane fade" id="userFollowers_<?= $cuser['id'] ?>"></div>
                    <div role="tabpanel" class="tab-pane fade" id="userFollowing_<?= $cuser['id'] ?>"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('#userNews_<?= $cuser['id']?>').load('<?=site_url('user/News/' . $cuser['id'])?>');

        if($(window).width()<768) $('.nav-justified').removeClass('nav-justified');

        $('#userProfileTabs<?= $cuser['id']?> a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var id = e.target.hash.split('_');
            var action = id[0].substr(5);
            if ($(e.target.hash).html() === '')
                $(e.target.hash).load('<?=site_url('user/')?>' + action + '/<?=$cuser['id']?>');
        })
    })
</script>