<div class="row">
    <div class="col-lg-8 hidden-xs">
        <!-- small box -->
        <div class="small-box bg-aqua announce">
            <div class="inner" style="max-height: 300px;overflow-y: auto;">
                <h3><?= $announce['title'] ?></h3> - <?= $announce['date'] ?>

                <p><?= $announce['content'] ?></p>
            </div>
            <div class="icon">
                <i class="fa fa-bullhorn"></i>
            </div>

        </div>
    </div>
    <div class="col-lg-4 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <p class="login-box-msg">
                    用户登录
                </p>
                <form action="<?= site_url('user/login') ?>" id="loginForm" method="post" role="form">
                    <div class="form-group">
                        <input class="form-control" data-required="true" name="username" placeholder="username" type="text">
                        </input>
                    </div>
                    <div class="form-group">
                        <input class="form-control" data-required="true" name="password" placeholder="Password" type="password">
                        </input>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <button class="btn btn-success btn-block" type="submit">
                                登 录
                            </button>
                            <button class="btn btn-default btn-block" id="register" type="button">
                                注册
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(function () {
        $('#register').click(function() {
            $.get('<?=site_url("user/register")?>',function(data) {
                $('#login-content').html(data)
            })
        })
        //提交表单
        $('#loginForm').validator({
            success: function (data) {
                showMessage(data.message);

                setTimeout(function () {
                    window.location.href = '<?= site_url('') ?>';
                }, 800);
            }
        });
    });
</script>
