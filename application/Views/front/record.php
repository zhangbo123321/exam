<div class="box box-danger box-collapse box-maximize">
    <div class="box-header with-border">
        <h3 class="box-title">记录</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body" id="authorBox">
        <table class="table table-striped" id="testTable"></table>
    </div>
    <!-- /.box-body -->
</div>


<link href="<?= $assets ?>/css/bootstrap-table.min.css" media="screen" rel="stylesheet" type="text/css"/>

<script type="text/javascript " src="<?php echo $assets ?>/js/bootstrap-table.min.js"></script>
<script type="text/javascript " src="<?php echo $assets ?>/js/bootstrap-table-locale-all.min.js "></script>
<script type="text/javascript " src="<?php echo $assets ?>/js/tableExport.js "></script>
<script type="text/javascript " src="<?php echo $assets ?>/js/bootstrap-table-export.min.js "></script>

<script type="text/javascript">
    $(function () {
        $('#testTable').bootstrapTable({
            url: '<?php echo site_url('test/record') ?>',
            locale: 'zh-CN',
            sortName: "start_time",
            sidePagination: "server",
            pageSize: 10,
            pageList: [10, 20, 50, 100],
            sortOrder: "DESC",
            selectItemName: "id",
            search: false,
            showColumns: false,
            showRefresh: false,
            columns: [{
                field: 'roomName',
                title: '考场',
                sortable: true
            }, {
                field: 'score',
                title: '得分',
                sortable: true
            }, {
                field: 'start_time',
                title: '考试时间',
                sortable: true,
                class: 'hidden-sm hidden-xs'
            }, {
                field: 'usetime',
                title: '用时',
                sortable: true,
                class: 'hidden-sm hidden-xs'
            }, {
                title: '排名',
                formatter: function (value, row) {
                    $.ajaxSetup("async", false);
                    var rank = 0;
                    $.ajax({
                        dataType: 'json',
                        url: "<?=site_url('test/rank')?>",
                        data: {room: row['room'], page: row['page'], user: row['user']},
                        async: false,//这里选择异步为false，那么这个程序执行到这里的时候会暂停，等待
                                     //数据加载完成后才继续执行
                        success: function (data) {
                            rank = data;
                        }
                    });

                    return rank;
                }
            }, {
                title: '查看',
                formatter: function (value, row) {
                    return "<button class='btn btn-xs viewTest' data-id='" + row['id'] + "' title='查看试卷'><i class='fa fa-eye'></i></button>&nbsp;";
                }
            }]
        });

        $('#testTable').on('click', '.viewTest', function () {
            var id = $(this).data('id');
            window.location.href = '<?=site_url("/test/view") ?>/' + id;
        })
    });
</script>