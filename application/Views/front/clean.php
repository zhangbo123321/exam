<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
    <meta content="" name="description"/>
    <meta content="" name="keywords"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
    <title>
        <?= $title ?>
    </title>
    <link href="<?= $assets ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= $assets ?>/css/icons.css" rel="stylesheet" type="text/css"/>
    <link href="<?= $assets ?>/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= $assets ?>/css/front.css" rel="stylesheet" type="text/css"/>
    <script src="<?= $assets ?>/js/jquery.min.js" type="text/javascript"></script>
    <script src="<?= $assets ?>/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?= $assets ?>/js/function.js" type="text/javascript"></script>
    </meta>
</head>
<body class="middle-content page-login">
<div class="login-box">
    <div class="login-logo">
        <a href="<?= site_url('') ?>">
            <img src="<?= $assets ?>/images/logo.png" class="img-responsive center-block"/>
        </a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body" id="login-content">
        <?= $content ?>
    </div>
</div>
<script src="<?= $assets ?>/js/bootstrap-notify.min.js" type="text/javascript"></script>
<script src="<?= $assets ?>/js/jquery.validator.js" type="text/javascript"></script>
</body>
</html>