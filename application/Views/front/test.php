<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
    <meta content="" name="description"/>
    <meta content="" name="keywords"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
    <title>
        <?= $title ?>
    </title>
    <link href="<?= $assets ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= $assets ?>/css/icons.css" rel="stylesheet" type="text/css"/>
    <link href="<?= $assets ?>/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= $assets ?>/css/_all-skins.min.css" media="screen" rel="stylesheet" type="text/css"/>
    <link href="<?= $assets ?>/css/skin-blue.min.css" media="screen" rel="stylesheet" type="text/css"/>
    <link href="<?= $assets ?>/css/front.css" rel="stylesheet" type="text/css"/>
    <link href="<?= $assets ?>/css/exam.css" media="screen" rel="stylesheet" type="text/css"/>
    <script src="<?= $assets ?>/js/jquery.min.js" type="text/javascript"></script>
    <script src="<?= $assets ?>/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?= $assets ?>/js/function.js" type="text/javascript"></script>
    </meta>
</head>
<body class="hold-transition skin-blue layout-top-nav">

<div class="wrapper">
    <header class="main-header">
        <nav class="navbar navbar-static-top navbar-fixed-top">
            <div class="container">
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <div class="collapse navbar-collapse pull-left">
                    <ul class="nav navbar-nav">
                        <li>
                            <a class="navbar-brand" id="testTitle">
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- Notifications Menu -->
                        <li class="notifications-menu">
                            <!-- Menu toggle button -->
                            <a style="font-size: 20px;">
                                <i class="fa icon-clock"> </i>
                                <span id="testTime"></span>
                            </a>
                        </li>
                        <!-- Tasks Menu -->
                        <!-- User Account Menu -->
                        <li>
                            <!-- Menu Toggle Button -->
                            <a id="saveTest" href="#">
                                <i class="fa fa-save"></i>
                            </a>
                        </li>

                        <li>
                            <!-- Menu Toggle Button -->
                            <a class="btn-success" id="submitTest" href="#">
                                <i class="icon-ok-circled"></i>
                                交卷
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar sidebar-fixed">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar" style="height: auto;">
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu tree" id="testMenu">
                <li class="header">题目一览</li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
    <div class="content-wrapper">
        <form class="form" action="<?=site_url('test/save')?>" method="post">
        <div class="content" id="testPage">

        </div>
        </form>
    </div>
</div>



<div id="cover" class="cover"></div>
<div id="loading" class="loading"><i class=" icon-spin4 animate-spin"></i> 正在加载试卷，请稍等...</div>

<script src="<?= $assets ?>/js/jquery.metisMenu.js" type="text/javascript"></script>
<script src="<?= $assets ?>/js/jquery.exam.js" type="text/javascript"></script>
<script src="<?= $assets ?>/js/jquery.validator.js" type="text/javascript"></script>

<script type="text/javascript">

    $(function () {

        $('[data-toggle=push-menu]').click(function () {
            if (parseInt($('.main-sidebar').css('left')) < 0) {
                $('.main-sidebar').css({'left': '0px'});
                $('.content-wrapper').css('margin-left', 230);
            } else {
                $('.main-sidebar').css({'left': '-230px'});
                $('.content-wrapper').css('margin-left', 0);
            }
        });
        //$('[data-toggle=push-menu]').click();

        $('#testPage').exam({
            'room':<?=$room?>,
            'page': <?=$page?>,
            'url': "<?=site_url('test/page')?>"
        })

    })
</script>
</body>
</html>