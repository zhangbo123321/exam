<div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>
                    <?=$users?>
                </h3>
                <p>
                    用户
                </p>
            </div>
            <div class="icon">
                <i class="fa fa-user">
                </i>
            </div>
            <a class="small-box-footer" href="<?=site_url('admin/users')?>">
                详细信息
                <i class="fa fa-arrow-circle-right">
                </i>
            </a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3>
                    <?=$pages?>
                </h3>
                <p>
                    试卷
                </p>
            </div>
            <div class="icon">
                <i class="icon icon-note">
                </i>
            </div>
            <a class="small-box-footer" href="<?=site_url('admin/page')?>">
                详细信息
                <i class="fa fa-arrow-circle-right">
                </i>
            </a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>
                    <?=$questions?>
                </h3>
                <p>
                    试题
                </p>
            </div>
            <div class="icon">
                <i class="fa fa-question-circle-o">
                </i>
            </div>
            <a class="small-box-footer" href="<?=site_url('admin/question')?>">
                详细信息
                <i class="fa fa-arrow-circle-right">
                </i>
            </a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3>
                    <?=$rooms?>
                </h3>
                <p>
                    考场
                </p>
            </div>
            <div class="icon">
                <i class="fa fa-th-large">
                </i>
            </div>
            <a class="small-box-footer" href="<?=site_url('admin/room')?>">
                详细信息
                <i class="fa fa-arrow-circle-right">
                </i>
            </a>
        </div>
    </div>
    <!-- ./col -->
</div>