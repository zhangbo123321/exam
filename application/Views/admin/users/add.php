<form class="form-horizontal" id="userForm" action="<?= site_url('/admin/users/save') ?>" method="post">


    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#addUser" aria-controls="addUser" role="tab" data-toggle="tab">必填项</a>
            </li>
            <li class="pull-right">
                <div class="btn-group btn-group-sm" role="group" aria-label="...">
                    <button type="submit" class="btn btn-primary" id="submit">
                        <i class="glyphicon glyphicon-floppy-disk"></i> 保存
                    </button>
                    <button type="reset" class="btn btn-info">
                        <i class="glyphicon glyphicon-retweet"></i> 重置
                    </button>
                </div>
            </li>
        </ul>

        <input type="hidden" name="id" value="<?= isset($editUser) ? $editUser['id'] : "" ?>"/>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="addUser">
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">名称</label>

                    <div class="col-sm-9 input-group">
                        <input type="text" class="form-control" id="username" placeholder="Username" name="username" data-required="true" value="<?= isset($editUser) ? $editUser['username'] : "" ?>"/>
                    </div>
                </div>


                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">密码</label>

                    <div class="col-sm-9 input-group">
                        <input type="password" class="form-control" id="password"
                               placeholder="Password 如果不填写将是<?= isset($editUser) ? "原密码" : "123" ?>"
                               name="password"
                               value=""/>
                    </div>
                </div>


                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">邮箱</label>

                    <div class="col-sm-9 input-group">
                        <input type="email" class="form-control" id="email" placeholder="Email" name="email" value="<?= isset($editUser) ? $editUser['email'] : "" ?>"/>
                    </div>
                </div>


                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">用户组</label>

                    <div class="col-sm-9 input-group">
                        <select class="form-control" id="group_id" name="group_id">
                            <?php foreach ($groups as $g): ?>
                                <option value="<?= $g['id'] ?>" <?= (isset($editUser) && $editUser['group'] == $g['id']) ? "selected" : "" ?> ><?= $g['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">用户组</label>

                    <div class="col-sm-9 input-group">
                        <select class="form-control show-tick" id="zone_id" name="zone_id">
                            <?php foreach ($zones as $c): ?>
                                <option value="<?= $c['id'] ?>" data-parent="<?=$c['parent']?>" <?= (isset($editUser) && $editUser['zone'] == $c['id']) ? "selected" : "" ?> ><?= $c['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>


            </div>
        </div>
    </div>

</form>

<link href="<?= $assets ?>/css/jquery.selectTree.css" media="screen" rel="stylesheet" type="text/css"/>

<script src="<?= $assets ?>/js/jquery.validator.js"></script>
<script src="<?= $assets ?>/js/jquery.selectTree.js"></script>


<script type="text/javascript">
    $(function () {

        $("#zone_id").select({
            selected: '<?=isset($editUser)?$editUser['zone']:''?>'
        });

        //提交表单
        $('#userForm').validator({
            success: function (data) {
                showMessage(data.message);
                if (data.status!='Error') {
                    dialog.close();
                    $('#usersTable').bootstrapTable('refresh');
                }
            },
            error: function () {
                showMessage("提交失败！");
            }

        });
    });
</script>