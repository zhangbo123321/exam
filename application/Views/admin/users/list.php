<div id="usersTableToolbar"></div>

<div class="box">
    <div class="box-header">
        <div id="usersToolbar" class="btn-group">
            <button type="button" class="btn btn-default" data-dialog="<?php echo site_url('/admin/users/add') ?>"
                    title="新建用户">
                <i class="icon-user-plus"></i>
            </button>
            <button type="button" class="btn btn-default" id="deleteSelected" title="删除所选">
                <i class="fa fa-trash"></i>
            </button>
            <button type="button" class="btn btn-default" id="trash" title="回收站">
                <i class="fa fa-recycle"></i>
            </button>
        </div>
    </div>

    <div class="box-body">
        <table class="table table-bordered table-striped" id="usersTable"></table>
    </div>
</div>

</div>

<link href="<?= $assets ?>/css/bootstrap-table.min.css" media="screen" rel="stylesheet" type="text/css"/>

<script type="text/javascript " src="<?php echo $assets ?>/js/bootstrap-table.min.js"></script>
<script type="text/javascript " src="<?php echo $assets ?>/js/bootstrap-table-locale-all.min.js "></script>
<script type="text/javascript " src="<?php echo $assets ?>/js/tableExport.js "></script>
<script type="text/javascript " src="<?php echo $assets ?>/js/bootstrap-table-export.min.js "></script>

<script type="text/javascript ">
    $(function () {
        $('#usersTable').bootstrapTable({
            url: '<?php echo site_url('/admin/users/table') ?>',
            locale: 'zh-CN',
            sortName: "id",
            sidePagination: "server",
            pageSize: 10,
            pageList: [10, 20, 50, 100],
            selectItemName: "id",
            toolbar: "#usersToolbar",
            toolbarTarget: '#usersTableToolbar',
            columns: [{
                checkbox: true
            }, {
                field: 'id',
                title: 'ID',
                clickToSelect: true,
                sortable: true
            }, {
                field: 'username',
                title: '用户名',
                clickToSelect: true,
                sortable: true
            }, {
                field: 'email',
                title: '邮箱',
                clickToSelect: true,
                sortable: true
            }, {
                field: 'groupName',
                title: '用户组',
                clickToSelect: true,
                sortable: true
            }, {
                field: "zoneName",
                title: "区域",
                clickToSelect: true,
                sortable: true
            }, {
                title: '操作',
                formatter: function (value, row) {
                    var str = "<button class='btn btn-xs editUser' data-dialog='<?php echo site_url("/admin/users/add/") ?>" + row['id'] +
                        "' title='编辑用户'><i class='fa fa-edit'></i></button>&nbsp;" +
                        "<button class='btn btn-xs deleteUser' id='" + row['id'] +
                        "' data-recover=false title='删除用户'><i class='fa fa-trash'></i></button>&nbsp;"
                    ;
                    if (row['deleted'] === "1") {
                        str += "<button class='btn btn-xs deleteUser' id='" + row['id'] + "' data-recover=true title='恢复用户'><i class='fa fa-repeat'></i></button> ";
                    }
                    return str;
                }
            }]
        });

        $('#usersTable').on('click', '.deleteUser', function () {
            var id = $(this).attr('id');
            var recover = $(this).data('recover');
            console.log(recover)
            $.post('<?=site_url('admin/users/delete/')?>' + recover, {id: id}, function (result) {
                var e = $.parseJSON(result);
                if (e.status == 'Error') {
                    showMessage({
                        title: e.status,
                        message: e.message,
                        type: 'danger'
                    });
                } else {
                    showMessage({
                        title: e.status,
                        message: e.message,
                        type: 'success'
                    });
                    $('#usersTable').bootstrapTable('refresh');
                }
            })
            return false;
        });

        $('#deleteSelected').click(function () {
            var ids = $.map($('#usersTable').bootstrapTable('getSelections'), function (row) {
                return row.id;
            });

            $.ajax({
                url: '<?=site_url('admin/users/delete/')?>',
                async: false,
                type: 'POST',
                timeout: 60000,
                data: {id: ids},
                success: function (result) {
                    var e = $.parseJSON(result);
                    if (e.status == 'Error') {
                        showMessage({
                            title: e.status,
                            message: e.message,
                            type: 'danger'
                        });
                    } else {
                        showMessage({
                            title: e.status,
                            message: e.message,
                            type: 'success'
                        });

                        $('#usersTable').bootstrapTable('refresh');
                    }
                },
                error: function (e) {
                    $('#mainContent').html(e.responseText);
                }
            });
        });

        $('#trash').click(function () {
            var trash = $('#usersTable').data('trash') == 1 ? 0 : 1;
            $('#usersTable').bootstrapTable('refresh', {url: '<?php echo site_url('/admin/users/table/') ?>' + trash});
            $('#usersTable').data('trash', trash);
        });

    })
</script>
