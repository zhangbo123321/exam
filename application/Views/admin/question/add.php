<div class="panel panel-default">
    <div class="panel-body">
        <form class="form-horizontal" id="questionFrom" action="<?= site_url('admin/question/save') ?>" method="post">

            <!-- Nav tabs -->

            <?php if (isset($question)): ?>
                <input type="hidden" name="id" value="<?= $question['id'] ?>"/>
            <?php endif; ?>

            <!-- Tab panes -->

            <div role="tabpanel" class="tab-pane active" id="home">
                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">类别</label>

                    <div class="col-sm-8">
                        <select name="category" id="selectCategory"></select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">题型</label>

                    <div class="col-sm-8">
                        <select name="type" class="form-control" id="type">
                            <?php foreach ($types as $type): ?>
                                <option value="<?= $type['id'] ?>" <?= isset($question) && $question['type'] == $type['id'] ? "selected" : '' ?>><?= $type['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">难易</label>

                    <div class="col-sm-8">
                        <select name="level" class="form-control">
                            <option value="0">易</option>
                            <option value="1">难</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">题干</label>

                    <div class="col-sm-8">
                            <textarea data-required="true" name="title" class="form-control">
                                <?= isset($question) ? trim($question['title']) : '' ?>
                            </textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">
                        选项/答案<br>

                        <div class="btn-group">
                            <button class="btn btn-sm" id="addOption" type="button">
                                <i class="fa fa-plus"></i>
                            </button>
                            <button class="btn btn-sm" id="deleteOption" type="button">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </label>

                    <div class="col-sm-8 form-inline">
                        <ol type="A" id="options">
                            <?php if (isset($question)) {
                                if ($question['type'] != 5) {
                                    switch ($question['type']) {
                                        case 1:
                                        case 3:
                                            $input = 'radio';
                                            break;
                                        case 2:
                                        case 4:
                                            $input = 'checkbox';
                                    }
                                    $i = 0;
                                    if (is_array(json_decode($question['option'], true))) {
                                        foreach (json_decode($question['option'], true) as $option) {
                                            ?>
                                            <li>
                                                <input type="text" name="option[]" value="<?= $option['t'] ?>" class="form-control" <?= $question['type'] == 3 ? 'readonly' : "" ?> style="width:70%"/>
                                                <input type="<?= $input ?>" name="answer[]" <?= (isset($option['a']) && $option['a'] == 't') ? "checked" : '' ?> class="form-control" value="<?= $i ?>"/>
                                            </li>
                                            <?php
                                            $i++;
                                        }
                                    }
                                } else {
                                    echo '<textarea name="option" class="form-control">';
                                    echo $question['option'];
                                    echo '</textarea>';
                                }
                            } else {
                                ?>
                                <li>
                                    <input type="text" name="option[]" class="form-control" style="width:70%"/>
                                    <input type="radio" name="answer[]" class="form-control" value="0"/>
                                </li>
                                <li>
                                    <input type="text" name="option[]" class="form-control" style="width:70%"/>
                                    <input type="radio" name="answer[]" class="form-control" value="1"/>
                                </li>
                                <li>
                                    <input type="text" name="option[]" class="form-control" style="width:70%"/>
                                    <input type="radio" name="answer[]" class="form-control" value="2"/>
                                </li>
                                <li>
                                    <input type="text" name="option[]" class="form-control" style="width:70%"/>
                                    <input type="radio" name="answer[]" class="form-control" value="3"/>
                                </li>
                            <?php } ?>
                        </ol>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">解析</label>

                    <div class="col-sm-8">
                            <textarea name="analysis" class="form-control">
                                <?= isset($question) ? $question['analysis'] : '' ?>
                            </textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary">保存</button>
                        <button type="reset" class="btn btn-default">重置</button>
                    </div>
                </div>
            </div>
    </div>
    </form>

</div>

<link rel="stylesheet" type="text/css" media="screen" href="<?= $assets ?>/css/summernote.css"/>
<link rel="stylesheet" type="text/css" media="screen" href="<?= $assets ?>/css/jquery.selectTree.css"/>

<script src="<?= $assets ?>/js/jquery.validator.js"></script>
<script src="<?= $assets ?>/js/jquery.selectTree.js"></script>
<script src="<?= $assets ?>/js/summernote.min.js"></script>
<script src="<?= $assets ?>/js/summernote-zh-CN.min.js"></script>

<script type="text/javascript">
    $(function () {

        $('textarea').summernote({
            'lang': 'zh-CN',
            'height': 100
        });

        $('#selectCategory').select({
            data: <?=json_encode($categories, JSON_UNESCAPED_UNICODE)?>,
            selected: '<?=isset($question) ? $question['category'] : ''?>'
        });

        $('#type').change(function () {
            var type = $(this).val();
            var options = $('#options');
            var text = $('#options').find(':text');
            options.html('');
            switch (type) {
                case '1':
                case '2':
                    var check = type == 1 ? 'radio' : 'checkbox'
                    text.each(function (k) {
                        var value = $(this).val();
                        var li = $('<li>').append(
                            $('<input>', {
                                'type': 'text',
                                'name': 'option[]',
                                'class': 'form-control',
                                'style': 'width:70%',
                                'value': value
                            })
                        ).append(
                            $('<input>', {'type': check, 'name': 'answer[]', 'class': 'form-control', 'value': k})
                        );
                        options.append(li);
                    });
                    break;
                case '3':
                    var li = $('<li>').append(
                        $('<input>', {
                            'type': 'text',
                            'name': 'option[]',
                            'class': 'form-control',
                            'value': '正确',
                            'style': 'width:70%',
                            'readonly': 'true'
                        })
                    ).append(
                        $('<input>', {
                            'type': 'radio',
                            'name': 'answer[]',
                            'class': 'form-control',
                            'value': 0,
                            'checked': true
                        })
                    );
                    options.append(li);
                    var li = $('<li>').append(
                        $('<input>', {
                            'type': 'text',
                            'name': 'option[]',
                            'class': 'form-control',
                            'value': '错误',
                            'style': 'width:70%',
                            'disabled': 'true'
                        })
                    ).append(
                        $('<input>', {'type': 'radio', 'name': 'answer[]', 'class': 'form-control', 'value': 1})
                    );
                    options.append(li);
                    break;
                case '4':
                    for (var i = 0; i < 2; i++) {
                        var li = $('<li>').append(
                            $('<input>', {
                                'type': 'text',
                                'name': 'option[]',
                                'class': 'form-control',
                                'style': 'width:70%'
                            })
                        ).append(
                            $('<input>', {
                                'type': 'checkbox',
                                'name': 'answer[]',
                                'class': 'form-control hide',
                                'value': i,
                                'checked': true
                            })
                        );
                        options.append(li);
                    }
                    break;
                case '5':
                    var text = $("<textarea>", {
                        'name': 'option',
                        'class': 'form-control',
                        'style': 'width:100%',
                        'placeholder': "请输入答案"
                    });
                    options.html('').append(text);
                    break;
            }
        });

        $('#addOption').click(function () {
            var size = $('#options').find('li').length;
            var type = $('select[name=type]').val();
            var choice = (type == 1 || type == 3) ? 'radio' : 'checkbox';

            if (type == 5) return;

            if (type == 3 && $('#options').find('li').length >= 2) return;

            var li = $('<li>').append(
                $('<input>', {
                    'type': 'text',
                    'name': 'option[]',
                    'class': 'form-control',
                    'style': 'width:70%'
                })
            ).append(
                $('<input>', {
                    'type': choice,
                    'name': 'answer[]',
                    'class': 'form-control',
                    'value': size
                })
            );
            $('#options').append(li);
        });

        $('#deleteOption').click(function () {
            var type = $('select[name=type]').val();
            if (type == 3 && $('#options').find('li').length == 2) return;
            $('#options li:last').remove();
        })

        $('#questionFrom').validator({
            success: function (data) {
                showMessage(data.message);
                if (data.status != 'Error') {
                    window.location.href = "<?=site_url('admin/question')?>"
                }
            }
        });

    });
</script>
