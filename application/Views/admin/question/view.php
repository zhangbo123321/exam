<div class="panel panel-default">
    <table class="table">
        <tr>
            <td width="200px" class="text-right">题干:</td>
            <td><h5><?=$question['title']?></h5></td>
        </tr>
        <tr>
            <td class="text-right">类型:</td>
            <td><?=$question['type_name']?></td>
        </tr>
        <tr>
            <td class="text-right">分类:</td>
            <td><?=$question['category_name'] ?: ""?></td>
        </tr>
        <tr>
            <td class="text-right">选项/答案:</td>
            <td>
                <ol type="A">
                <?php
$options = json_decode($question['option'], true);

foreach ($options as $option) {
    echo "<li>" . $option['t'];
    if (isset($option['a']) && $option['a']) {
        echo "<i class='fa fa-check'></i>";
    }

    echo "</li>";
}
?>
                </ol>
            </td>
        </tr>
        <tr>
            <td class="text-right">难易:</td>
            <td><?=$question['level'] == 0 ? '易' : '难'?></td>
        </tr>
        <tr>
            <td class="text-right">解析:</td>
            <td><?=$question['analysis']?></td>
        </tr>
    </table>
</div>
