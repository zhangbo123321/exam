<div id="usersTableToolbar"></div>

<div class="box">
    <div class="box-header">
        <div id="usersToolbar" class="btn-group">
            <a type="button" class="btn btn-default" data-pjax="true" href="<?= site_url('/admin/question/add') ?>"
               title="新建试题">
                <i class="icon-plus"></i>
            </a>
            <button type="button" class="btn btn-default" data-dialog="<?= site_url('/admin/question/import') ?>" title="导入试题">
                <i class="fa fa-download"></i>
            </button>
            <button type="button" class="btn btn-default" id="deleteSelected" title="删除所选">
                <i class="fa fa-trash"></i>
            </button>
            <button type="button" class="btn btn-default" id="trash" title="回收站">
                <i class="fa fa-recycle"></i>
            </button>
        </div>
    </div>

    <div class="box-body">
        <table class="table table-bordered table-striped" id="questionsTable"></table>
    </div>
</div>

</div>

<link href="<?= $assets ?>/css/bootstrap-table.min.css" media="screen" rel="stylesheet" type="text/css"/>

<script type="text/javascript " src="<?php echo $assets ?>/js/bootstrap-table.min.js"></script>
<script type="text/javascript " src="<?php echo $assets ?>/js/bootstrap-table-locale-all.min.js "></script>
<script type="text/javascript " src="<?php echo $assets ?>/js/tableExport.js "></script>
<script type="text/javascript " src="<?php echo $assets ?>/js/bootstrap-table-export.min.js "></script>

<script type="text/javascript ">
    $(function () {
        $('#questionsTable').bootstrapTable({
            url: '<?php echo site_url('admin/question/table') ?>',
            locale: 'zh-CN',
            sortName: "id",
            sidePagination: "server",
            pageSize: 10,
            pageList: [10, 20, 50, 100],
            selectItemName: "id",
            sortOrder: 'desc',
            toolbar: "#usersToolbar",
            toolbarTarget: '#usersTableToolbar',
            showColumns: false,
            showRefresh: false,
            columns: [{
                checkbox: true
            }, {
                field: 'id',
                title: 'ID',
                clickToSelect: true,
                sortable: true,

            }, {
                field: 'categoryName',
                title: '分类',
                clickToSelect: true,
                sortable: true
            }, {
                field: 'title',
                title: '题干',
                formatter: function (value, row) {
                    return "<a href='#' title='查看试题' data-dialog='<?=site_url("admin/question/view")?>/" + row['id'] + "'>" + value + "</a>"
                }
            }, {
                field: 'typeName',
                title: '类型',
                clickToSelect: true,
                sortable: true
            }, {
                field: "level",
                title: "难易",
                formatter: function (value) {
                    return value == 0 ? '易' : '难'
                }
            }, {
                title: '操作',
                width: '80px',
                formatter: function (value, row) {
                    var str = "<button class='btn btn-xs' data-pjax='true' href='<?php echo site_url("/admin/question/add/") ?>" + row['id'] +
                        "' title='编辑试题'><i class='fa fa-edit'></i></button>&nbsp;" +
                        "<button class='btn btn-xs deleteQuestion' id='" + row['id'] +
                        "' data-recover=false title='删除试题'><i class='fa fa-trash'></i></button>&nbsp;"
                    ;
                    if (row['deleted'] === "1") {
                        str += "<button class='btn btn-xs deleteQuestion' id='" + row['id'] + "' data-recover=true title='恢复用户'><i class='fa fa-repeat'></i></button> ";
                    }
                    return str;
                }
            }]
        });

        $('#questionsTable').on("dbl-click-row.bs.table", function (e, row) {
            dialog("<?=site_url('admin/question/view')?>/" + row.id);
        });

        $('#questionsTable').on('click', '.deleteQuestion', function () {
            var id = $(this).attr('id');
            var trash = $('#questionsTable').data('trash')
            $.post('<?=site_url('admin/question/delete')?>', {id: id, trash: trash}, function (result) {
                var e = $.parseJSON(result);
                showMessage(e.message)
                if (e.status != 'Error') {
                    $('#questionsTable').bootstrapTable('refresh');
                }
            })
            return false;
        });

        $('#deleteSelected').click(function () {
            var ids = $.map($('#questionsTable').bootstrapTable('getSelections'), function (row) {
                return row.id;
            });

            var trash = $('#questionsTable').data('trash')

            $.ajax({
                url: '<?=site_url('admin/question/delete')?>',
                async: false,
                type: 'POST',
                timeout: 60000,
                data: {id: ids, trash: trash},
                success: function (result) {
                    var e = $.parseJSON(result);
                    showMessage(e.message)
                    if (e.status != 'Error') {
                        $('#questionsTable').bootstrapTable('refresh');
                    }
                },
                error: function (e) {
                    $('#mainContent').html(e.responseText);
                }
            });
        });

        $('#trash').click(function () {
            var trash = $('#questionsTable').data('trash') == 1 ? 0 : 1;
            $('#questionsTable').bootstrapTable('refresh', {url: '<?php echo site_url('/admin/question/table/') ?>' + trash});
            $('#questionsTable').data('trash', trash);
        });

    })
</script>
