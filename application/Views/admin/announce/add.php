<form class="form-horizontal" id="announceForm" action="<?= site_url('admin/announce/save') ?>" method="post">
    <input type="hidden" name="id" value="<?= isset($announce) ? $announce['id'] : 0 ?>" />
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">标题</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="title" placeholder="Title" value="<?= isset($announce) ? $announce['title'] : '' ?>"/>
        </div>
    </div>
    <div class="form-group">
        <label for="inputPassword3" class="col-sm-2 control-label">内容</label>
        <div class="col-sm-10">
            <textarea name="content" class="form-control"><?= isset($announce) ? $announce['content'] : '' ?></textarea>
        </div>
    </div>

    <div class="form-group">
        <div class="text-right" style="margin-right: 10px;">
            <button type="button" class="btn btn-default" onclick="dialog.close();">取消</button>
            <button type="submit" class="btn btn-primary">保存</button>
        </div>
    </div>
</form>

<link href="<?= $assets ?>/css/summernote.css" media="screen" rel="stylesheet" type="text/css"/>
<script src="<?= $assets ?>/js/jquery.validator.js"></script>
<script src="<?= $assets ?>/js/summernote.min.js"></script>
<script src="<?= $assets ?>/js/summernote-zh-CN.min.js"></script>
<script type="text/javascript">
    $(function () {

        $('[name=content]').summernote({
            height:200,
            lang:"zh-CN",
            dialogsInBody:true
        });
        //提交表单
        $('#announceForm').validator({
            success: function (data) {
                showMessage(data.message);
                if (data.status != 'Error') {
                    dialog.close();
                    window.location.reload();
                }
            },
            error: function () {
                showMessage("提交失败！");
            }
        });
    });
</script>