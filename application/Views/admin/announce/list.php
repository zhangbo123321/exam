<div class="box">
    <div class="box-header">
        <div id="usersToolbar" class="btn-group pull-right">
            <button type="button" class="btn btn-default" data-dialog="<?= site_url('/admin/announce/add') ?>"
                    title="新建公告">
                <i class="icon-plus"></i>
            </button>
        </div>
    </div>

    <div class="box-body">
        <div id="announceContent">
            <?php foreach ($announces as $an): ?>
                <div class="box box-primary box-collapse" data-id="<?= $an['id'] ?>">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= $an['title'] ?> </h3> - <?= $an['date'] ?>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool editAnnounce" title="编辑公告"><i class="fa fa-pencil"></i></button>
                            <button class="btn btn-box-tool removeAnnounce" title="删除公告"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <?= $an['content'] ?>
                    </div>
                    <!-- /.box-body -->
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="box-footer">
        <div id="announcePage">
        </div>
    </div>
</div>


<script src="<?= $assets ?>/js/jquery.pagination.js"></script>

<script type="text/javascript ">
    $(function () {
        $('.editAnnounce').click(function () {
            var $id = $(this).parents(".box:first").data("id");
            dialog("<?= site_url('/admin/announce/add') ?>/" + $id);
        });
        $('#announceContent').on("click", '.removeAnnounce', function () {
            var $id = $(this).parents(".box:first").data("id");
            confirm({
                message: "确认删除此公告",
                url: "<?=site_url('admin/announce/delete')?>/" + $id,
                success: function () {
                    $('[data-id=' + $id + ']').remove();
                }
            });
        });

        $('#announcePage').pagination({
            'total': <?=$allAnnounce?>,
            'url': '<?=site_url("admin/announce/page")?>',
            'target': "#announceContent"
        })
    })
</script>
