<?php foreach ($announces as $an): ?>
    <div class="box box-primary box-collapse box-remove" data-id="<?= $an['id'] ?>">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $an['title'] ?> </h3> - <?= $an['date'] ?>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool editAnnounce" title="编辑公告"><i class="fa fa-pencil"></i></button>
                <button class="btn btn-box-tool removeAnnounce" title="删除公告"><i class="fa fa-trash"></i></button>
            </div>
        </div>

        <!-- /.box-header -->
        <div class="box-body" id="authorBox">
            <?= $an['content'] ?>
        </div>
        <!-- /.box-body -->
    </div>
<?php endforeach; ?>