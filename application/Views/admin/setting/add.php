<form action="<?=site_url('admin/setting/save')?>" class="form-horizontal" id="settingForm" method="post">
    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li class="active" role="presentation">
                <a aria-controls="addSetting" data-toggle="tab" href="#addSetting" role="tab">
                    必填项
                </a>
            </li>
            <li class="pull-right">
                <div aria-label="..." class="btn-group btn-group-sm" role="group">
                    <button class="btn btn-primary" id="submit" type="submit">
                        <i class="glyphicon glyphicon-floppy-disk">
                        </i>
                    </button>
                    <button class="btn btn-info" type="reset">
                        <i class="glyphicon glyphicon-retweet">
                        </i>
                    </button>
                </div>
            </li>
        </ul>
        <input name="id" type="hidden" value="<?=isset($setting) ? $setting['id'] : ''?>"/>
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active" id="addSetting" role="tabpanel">
                <div class="form-group">
                    <label class="col-sm-2 text-right">
                        名称
                    </label>
                    <div class="col-sm-9 input-group">
                        <input class="form-control" data-required="true" name="name" placeholder="Name" type="text" value="<?=isset($setting) ? $setting['name'] : ''?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 text-right">
                        描述
                    </label>
                    <div class="col-sm-9 input-group">
                        <input class="form-control" data-required="true" name="desc" placeholder="Desc" type="text" value="<?=isset($setting) ? $setting['desc'] : ''?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 text-right">
                        类型
                    </label>
                    <div class="col-sm-9 input-group">
                        <select class="form-control" id="selectType" name="type">
                            <option selected="" value="text">
                                文本
                            </option>
                            <option value="textarea">
                                文本框
                            </option>
                            <option value="select">
                                选择项
                            </option>
                            <option value="number">
                                数字
                            </option>
                            <option value="date">
                                日期
                            </option>
                            <option value="datetime">
                                日期时间
                            </option>
                            <option value="url">
                                网址
                            </option>
                            <option value="email">
                                邮箱
                            </option>
                        </select>
                    </div>
                </div>
                <div class="form-group hide" id="inputOptions">
                    <label class="col-sm-2 text-right">
                        选项
                    </label>
                    <div class="col-sm-9 input-group">
                        <input class="form-control" data-required="true" data-role="tagsinput" name="options" placeholder="请输入选项，使用逗号分割" type="text" value="<?=isset($setting) ? $setting['options'] : ''?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 text-right">
                        值
                    </label>
                    <div class="col-sm-9 input-group">
                        <textarea class="form-control" data-required="true" name="value" placeholder="Value">
                            <?=isset($setting) ? $setting['desc'] : ''?>
                        </textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<link href="<?=$assets?>/css/bootstrap-tagsinput.css" media="screen" rel="stylesheet" type="text/css"/>
<script src="<?=$assets?>/js/jquery.validator.js">
</script>
<script src="<?=$assets?>/js/bootstrap-tagsinput.min.js">
</script>
<script type="text/javascript">
    $(function () {

        $('#selectType').change(function () {
            if ($(this).val() == 'select') {
                $('#inputOptions').removeClass('hide');
            } else {
                $('#inputOptions').addClass('hide');
            }
        });

        //提交表单
        $('#settingForm').validator({
            sending: {
                type: 'ajax',
                success: function (data) {
                    var e = $.parseJSON(data);
                    if (e.error) {
                        showMessage(e.message);
                    } else {
                        showMessage(e.message);
                        dialog.close();
                        var str = '<tr>\n' +
                            '<td>' + $('input[name=name]').val() + '</td>\n' +
                            '<td><a href="#" id="desc" data-type="text" data-pk="' + e.pk + '" data-title="输入描述">' + $('input[name=desc]').val() + '</td>\n' +
                            '<td><a href="#" id="value" data-type="' + $('select[name=type]').val() + '" data-pk="' + e.pk + '" data-title="输入' + $('input[name=desc]').val() + '">' + $('textarea').val() + '</a></td>\n' +
                            '</tr>';
                        $('#settingTable tbody').append(str);
                    }
                },
                error: function () {
                    showMessage("提交失败！");
                }
            }
        });
    });
</script>