<!-- IN-PLACE EDITING -->

<div class="row">
    <div class="col-xs-6">
        <label class="switch-input pull-left">
            <input type="checkbox" id="enableEditSetting" name="editSwitch">
            <i data-swon-text="ON" data-swoff-text="OFF"></i>
            允许编辑
        </label>
        <label class="switch-input pull-left" style="margin-left: 15px;">
            <input type="checkbox" id="switch-inline-input" name="switch-checkbox">
            <i data-swon-text="ON" data-swoff-text="OFF"></i>
            行内编辑
        </label>
    </div>
    <div class="col-xs-6 text-right">
        <button class="btn btn-default btn-sm" data-dialog="<?=site_url('admin/setting/add')?>" title="新增设置">
            <i class="icon-plus"></i> 新增设置
        </button>
    </div>
</div>
<div class="box">
    <div class="box-body no-padding">
        <table id="settingTable" class="table table-hover">
            <thead>
            <tr class="text-center">
                <th>名称</th>
                <th>描述</th>
                <th>值</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($settings as $s): ?>
                <tr>
                    <td><?=$s['name']?></td>
                    <td><a href="#" id="desc" data-type="text" data-pk="<?=$s['id']?>" data-title="输入描述"><?=$s['desc']?></td>
                    <td><a href="#" id="value" data-type="<?=$s['type']?>" data-pk="<?=$s['id']?>" <?php if ($s['options']){?>data-source='<?=$s['options']?>'<?php }?> data-title="输入<?=$s['desc']?>"><?=$s['value']?></a></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript" src="<?=$assets?>/js/bootstrap-editable.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        var editSetting = $('#settingTable a');

        editSetting.editable({
            url: '<?=site_url("admin/setting/save")?>',
            disabled: true
        });

        $('#enableEditSetting').click(function () {
            editSetting.editable('toggleDisabled');
        });

        $('#switch-inline-input').change(function () {
            console.log($(this).prop('checked')==true);
            if ($(this).prop('checked')==true) {
                editSetting.editable('option', 'mode', 'inline');
            } else {
                editSetting.editable('option', 'mode', 'popup');
            }
        });


    });
</script>