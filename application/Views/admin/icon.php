<!-- Start .page-content-inner -->
<div class="row icons">
    <!-- Start .row -->
    <div class="col-md-12">
        <div class="well">
            <div>
                <form>
                    <input type="text" class="form-control" name="searchIcon" placeholder="Search for icons ...">
                </form>
            </div>
        </div>

        <div class="box box-success box-collapse">
            <!-- Start .widget -->
            <div class="box-header with-border">
                <h3 class="box-title">Glyphicons</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-glass"></i> glyphicon-glass</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-music"></i> glyphicon-music</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-search"></i> glyphicon-search</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-envelope"></i> glyphicon-envelope</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-heart"></i> glyphicon-heart</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-star"></i> glyphicon-star</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-star-empty"></i> glyphicon-star-empty</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-user"></i> glyphicon-user</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-film"></i> glyphicon-film</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-th-large"></i> glyphicon-th-large</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-th"></i> glyphicon-th</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-th-list"></i> glyphicon-th-list</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-ok"></i> glyphicon-ok</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-remove"></i> glyphicon-remove</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-zoom-in"></i> glyphicon-zoom-in</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-zoom-out"></i> glyphicon-zoom-out</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-off"></i> glyphicon-off</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-signal"></i> glyphicon-signal</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-cog"></i> glyphicon-cog</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-trash"></i> glyphicon-trash</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-home"></i> glyphicon-home</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-file"></i> glyphicon-file</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-time"></i> glyphicon-time</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-road"></i> glyphicon-road</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-download-alt"></i> glyphicon-download-alt</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-download"></i> glyphicon-download</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-upload"></i> glyphicon-upload</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-inbox"></i> glyphicon-inbox</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-play-circle"></i> glyphicon-play-circle</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-repeat"></i> glyphicon-repeat</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-refresh"></i> glyphicon-refresh</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-list-alt"></i> glyphicon-list-alt</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-lock"></i> glyphicon-lock></div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-flag"></i> glyphicon-flag</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-headphones"></i> glyphicon-headphones</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-volume-off"></i> glyphicon-volume-off</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-volume-down"></i> glyphicon-volume-down</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-volume-up"></i> glyphicon-volume-up</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-qrcode"></i> glyphicon-qrcode</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-barcode"></i> glyphicon-barcode</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-tag"></i> glyphicon-tag</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-tags"></i> glyphicon-tags</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-book"></i> glyphicon-book</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-bookmark"></i> glyphicon-bookmark></div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-print"></i> glyphicon-print</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-camera"></i> glyphicon-camera></div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-font"></i> glyphicon-font</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-bold"></i> glyphicon-bold</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-italic"></i> glyphicon-italic</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-text-height"></i> glyphicon-text-height</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-text-width"></i> glyphicon-text-width</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-align-left"></i> glyphicon-align-left</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-align-center"></i> glyphicon-align-center</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-align-right"></i> glyphicon-align-right</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-align-justify"></i> glyphicon-align-justify</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-list"></i> glyphicon-list</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-indent-left"></i> glyphicon-indent-left</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-indent-right"></i> glyphicon-indent-right</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-facetime-video"></i> glyphicon-facetime-video</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-picture"></i> glyphicon-picture</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-pencil"></i> glyphicon-pencil</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-map-marker"></i> glyphicon-map-marker</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-adjust"></i> glyphicon-adjust</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-tint"></i> glyphicon-tint</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-edit"></i> glyphicon-edit</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-share"></i> glyphicon-share</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-check"></i> glyphicon-check</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-move"></i> glyphicon-move</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-step-backward"></i> glyphicon-step-backward</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-fast-backward"></i> glyphicon-fast-backward</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-backward"></i> glyphicon-backward</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-play"></i> glyphicon-play</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-pause"></i> glyphicon-pause</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-stop"></i> glyphicon-stop</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-forward"></i> glyphicon-forward</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-fast-forward"></i> glyphicon-fast-forward</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-step-forward"></i> glyphicon-step-forward</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-eject"></i> glyphicon-eject</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-chevron-left"></i> glyphicon-chevron-left</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-chevron-right"></i> glyphicon-chevron-right</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-plus-sign"></i> glyphicon-plus-sign</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-minus-sign"></i> glyphicon-minus-sign</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-remove-sign"></i> glyphicon-remove-sign</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-ok-sign"></i> glyphicon-ok-sign</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-question-sign"></i> glyphicon-question-sign</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-info-sign"></i> glyphicon-info-sign</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-screenshot"></i> glyphicon-screenshot</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-remove-circle"></i> glyphicon-remove-circle</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-ok-circle"></i> glyphicon-ok-circle</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-ban-circle"></i> glyphicon-ban-circle</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-arrow-left"></i> glyphicon-arrow-left</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-arrow-right"></i> glyphicon-arrow-right</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-arrow-up"></i> glyphicon-arrow-up</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-arrow-down"></i> glyphicon-arrow-down</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-share-alt"></i> glyphicon-share-alt</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-resize-full"></i> glyphicon-resize-full</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-resize-small"></i> glyphicon-resize-small</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-plus"></i> glyphicon-plus</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-minus"></i> glyphicon-minus</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-asterisk"></i> glyphicon-asterisk</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-exclamation-sign"></i> glyphicon-exclamation-sign</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-gift"></i> glyphicon-gift</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-leaf"></i> glyphicon-leaf</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-fire"></i> glyphicon-fire</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-eye-open"></i> glyphicon-eye-open</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-eye-close"></i> glyphicon-eye-close</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-warning-sign"></i> glyphicon-warning-sign</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-plane"></i> glyphicon-plane</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-calendar"></i> glyphicon-calendar</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-random"></i> glyphicon-random</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-comment"></i> glyphicon-comment</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-magnet"></i> glyphicon-magnet</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-chevron-up"></i> glyphicon-chevron-up</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-chevron-down"></i> glyphicon-chevron-down</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-retweet"></i> glyphicon-retweet</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-shopping-cart"></i> glyphicon-shopping-cart</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-folder-close"></i> glyphicon-folder-close</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-folder-open"></i> glyphicon-folder-open</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-resize-vertical"></i> glyphicon-resize-vertical</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-resize-horizontal"></i> glyphicon-resize-horizontal</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-hdd"></i> glyphicon-hdd</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-bullhorn"></i> glyphicon-bullhorn</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-bell"></i> glyphicon-bell</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-certificate"></i> glyphicon-certificate</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-thumbs-up"></i> glyphicon-thumbs-up</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-thumbs-down"></i> glyphicon-thumbs-down</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-hand-right"></i> glyphicon-hand-right</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-hand-left"></i> glyphicon-hand-left</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-hand-up"></i> glyphicon-hand-up</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-hand-down"></i> glyphicon-hand-down</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-circle-arrow-right"></i> glyphicon-circle-arrow-right</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-circle-arrow-left"></i> glyphicon-circle-arrow-left</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-circle-arrow-up"></i> glyphicon-circle-arrow-up</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-circle-arrow-down"></i> glyphicon-circle-arrow-down</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-globe"></i> glyphicon-globe</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-wrench"></i> glyphicon-wrench</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-tasks"></i> glyphicon-tasks</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-filter"></i> glyphicon-filter</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-briefcase"></i> glyphicon-briefcase</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-fullscreen"></i> glyphicon-fullscreen</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-dashboard"></i> glyphicon-dashboard</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-paperclip"></i> glyphicon-paperclipn</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-heart-empty"></i> glyphicon-heart-empty</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-link"></i> glyphicon-link</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-phone"></i> glyphicon-phone</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-pushpin"></i> glyphicon-pushpin</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-euro"></i> glyphicon-euro</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-usd"></i> glyphicon-usd</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-gbp"></i> glyphicon-gbp</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-sort"></i> glyphicon-sort</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-sort-by-alphabet"></i> glyphicon-sort-by-alphabet</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-sort-by-alphabet-alt"></i> glyphicon-sort-by-alphabet-alt</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-sort-by-order"></i> glyphicon-sort-by-order</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-sort-by-order-alt"></i> glyphicon-sort-by-order-alt</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-sort-by-attributes"></i> glyphicon-sort-by-attributes</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-sort-by-attributes-alt"></i> glyphicon-sort-by-attributes-alt</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-unchecked"></i> glyphicon-unchecked</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-expand"></i> glyphicon-expand</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-collapse-down"></i> glyphicon-collapse-down</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-collapse-up"></i> glyphicon-collapse-top</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-log-in"></i> glyphicon-log-in</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-flash"></i> glyphicon-flash</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-log-out"></i> glyphicon-log-out</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-new-window"></i> glyphicon-new-window</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-record"></i> glyphicon-record</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-save"></i> glyphicon-save</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-open"></i> glyphicon-open</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-saved"></i> glyphicon-saved</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-import"></i> glyphicon-import</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-export"></i> glyphicon-export</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-send"></i> glyphicon-send</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-floppy-disk"></i> glyphicon-floppy-disk</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-floppy-saved"></i> glyphicon-floppy-saved</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-floppy-remove"></i> glyphicon-floppy-remove</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-floppy-save"></i> glyphicon-floppy-save</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-floppy-open"></i> glyphicon-floppy-open</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-credit-card"></i> glyphicon-credit-card</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-transfer"></i> glyphicon-transfer</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-cutlery"></i> glyphicon-cutlery</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-header"></i> glyphicon-header</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-compressed"></i> glyphicon-compressed</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-earphone"></i> glyphicon-earphone</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-phone-alt"></i> glyphicon-phone-alt</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-tower"></i> glyphicon-tower</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-stats"></i> glyphicon-stats</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-sd-video"></i> glyphicon-sd-video</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-hd-video"></i> glyphicon-hd-video</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-subtitles"></i> glyphicon-subtitles</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-sound-stereo"></i> glyphicon-sound-stereo</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-sound-dolby"></i> glyphicon-sound-dolby</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-sound-5-1"></i> glyphicon-sound-5-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-sound-6-1"></i> glyphicon-sound-6-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-sound-7-1"></i> glyphicon-sound-7-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-copyright-mark"></i> glyphicon-copyright-mark</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-registration-mark"></i> glyphicon-registration-mark</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-cloud"></i> glyphicon-cloud</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-cloud-download"></i> glyphicon-cloud-download</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-cloud-upload"></i> glyphicon-cloud-upload</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-tree-conifer"></i> glyphicon-tree-conifer</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="glyphicon glyphicon-tree-deciduous"></i> glyphicon-tree-deciduous</div>
                </div>
            </div>
        </div>

        <!-- End .widget -->
        <div class="box">
            <!-- Start .widget -->
            <div class="box-header with-border">
                <h3 class="box-title">Font awesome pack</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-box="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-address-book"></i> fa-address-book</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-address-book-o"></i> fa-address-book-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-address-card"></i> fa-address-card</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-address-card-o"></i> fa-address-card-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-adjust"></i> fa-adjust</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-american-sign-language-interpreting"></i> fa-american-sign-language-interpreting</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-anchor"></i> fa-anchor</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-archive"></i> fa-archive</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-area-chart"></i> fa-area-chart</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-arrows"></i> fa-arrows</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-arrows-h"></i> fa-arrows-h</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-arrows-v"></i> fa-arrows-v</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-asl-interpreting"></i> fa-asl-interpreting</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-assistive-listening-systems"></i> fa-assistive-listening-systems</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-asterisk"></i> fa-asterisk</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-at"></i> fa-at</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-audio-description"></i> fa-audio-description</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-automobile"></i> fa-automobile</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-balance-scale"></i> fa-balance-scale</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-ban"></i> fa-ban</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-bank"></i> fa-bank</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-bar-chart"></i> fa-bar-chart</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-bar-chart-o"></i> fa-bar-chart-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-barcode"></i> fa-barcode</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-bars"></i> fa-bars</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-bath"></i> fa-bath</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-bathtub"></i> fa-bathtub</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-battery"></i> fa-battery</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-battery-0"></i> fa-battery-0</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-battery-1"></i> fa-battery-1</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-battery-2"></i> fa-battery-2</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-battery-3"></i> fa-battery-3</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-battery-4"></i> fa-battery-4</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-battery-empty"></i> fa-battery-empty</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-battery-full"></i> fa-battery-full</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-battery-half"></i> fa-battery-half</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-battery-quarter"></i> fa-battery-quarter</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-battery-three-quarters"></i> fa-battery-three-quarters</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-bed"></i> fa-bed</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-beer"></i> fa-beer</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-bell"></i> fa-bell</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-bell-o"></i> fa-bell-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-bell-slash"></i> fa-bell-slash</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-bell-slash-o"></i> fa-bell-slash-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-bicycle"></i> fa-bicycle</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-binoculars"></i> fa-binoculars</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-birthday-cake"></i> fa-birthday-cake</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-blind"></i> fa-blind</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-bluetooth"></i> fa-bluetooth</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-bluetooth-b"></i> fa-bluetooth-b</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-bolt"></i> fa-bolt</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-bomb"></i> fa-bomb</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-book"></i> fa-book</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-bookmark"></i> fa-bookmark</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-bookmark-o"></i> fa-bookmark-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-braille"></i> fa-braille</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-briefcase"></i> fa-briefcase</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-bug"></i> fa-bug</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-building"></i> fa-building</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-building-o"></i> fa-building-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-bullhorn"></i> fa-bullhorn</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-bullseye"></i> fa-bullseye</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-bus"></i> fa-bus</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-cab"></i> fa-cab</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-calculator"></i> fa-calculator</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-calendar"></i> fa-calendar</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-calendar-check-o"></i> fa-calendar-check-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-calendar-minus-o"></i> fa-calendar-minus-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-calendar-o"></i> fa-calendar-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-calendar-plus-o"></i> fa-calendar-plus-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-calendar-times-o"></i> fa-calendar-times-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-camera"></i> fa-camera</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-camera-retro"></i> fa-camera-retro</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-car"></i> fa-car</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-caret-square-o-down"></i> fa-caret-square-o-down</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-caret-square-o-left"></i> fa-caret-square-o-left</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-caret-square-o-right"></i> fa-caret-square-o-right</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-caret-square-o-up"></i> fa-caret-square-o-up</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-cart-arrow-down"></i> fa-cart-arrow-down</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-cart-plus"></i> fa-cart-plus</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-cc"></i> fa-cc</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-certificate"></i> fa-certificate</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-check"></i> fa-check</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-check-circle"></i> fa-check-circle</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-check-circle-o"></i> fa-check-circle-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-check-square"></i> fa-check-square</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-check-square-o"></i> fa-check-square-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-child"></i> fa-child</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-circle"></i> fa-circle</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-circle-o"></i> fa-circle-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-circle-o-notch"></i> fa-circle-o-notch</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-circle-thin"></i> fa-circle-thin</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-clock-o"></i> fa-clock-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-clone"></i> fa-clone</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-close"></i> fa-close</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-cloud"></i> fa-cloud</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-cloud-download"></i> fa-cloud-download</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-cloud-upload"></i> fa-cloud-upload</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-code"></i> fa-code</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-code-fork"></i> fa-code-fork</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-coffee"></i> fa-coffee</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-cog"></i> fa-cog</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-cogs"></i> fa-cogs</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-comment"></i> fa-comment</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-comment-o"></i> fa-comment-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-commenting"></i> fa-commenting</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-commenting-o"></i> fa-commenting-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-comments"></i> fa-comments</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-comments-o"></i> fa-comments-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-compass"></i> fa-compass</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-copyright"></i> fa-copyright</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-creative-commons"></i> fa-creative-commons</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-credit-card"></i> fa-credit-card</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-credit-card-alt"></i> fa-credit-card-alt</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-crop"></i> fa-crop</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-crosshairs"></i> fa-crosshairs</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-cube"></i> fa-cube</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-cubes"></i> fa-cubes</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-cutlery"></i> fa-cutlery</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-dashboard"></i> fa-dashboard</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-database"></i> fa-database</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-deaf"></i> fa-deaf</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-deafness"></i> fa-deafness</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-desktop"></i> fa-desktop</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-diamond"></i> fa-diamond</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-dot-circle-o"></i> fa-dot-circle-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-download"></i> fa-download</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-drivers-license"></i> fa-drivers-license</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-drivers-license-o"></i> fa-drivers-license-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-edit"></i> fa-edit</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-ellipsis-h"></i> fa-ellipsis-h</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-ellipsis-v"></i> fa-ellipsis-v</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-envelope"></i> fa-envelope</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-envelope-o"></i> fa-envelope-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-envelope-open"></i> fa-envelope-open</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-envelope-open-o"></i> fa-envelope-open-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-envelope-square"></i> fa-envelope-square</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-eraser"></i> fa-eraser</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-exchange"></i> fa-exchange</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-exclamation"></i> fa-exclamation</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-exclamation-circle"></i> fa-exclamation-circle</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-exclamation-triangle"></i> fa-exclamation-triangle</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-external-link"></i> fa-external-link</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-external-link-square"></i> fa-external-link-square</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-eye"></i> fa-eye</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-eye-slash"></i> fa-eye-slash</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-eyedropper"></i> fa-eyedropper</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-fax"></i> fa-fax</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-feed"></i> fa-feed</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-female"></i> fa-female</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-fighter-jet"></i> fa-fighter-jet</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-file-archive-o"></i> fa-file-archive-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-file-audio-o"></i> fa-file-audio-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-file-code-o"></i> fa-file-code-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-file-excel-o"></i> fa-file-excel-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-file-image-o"></i> fa-file-image-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-file-movie-o"></i> fa-file-movie-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-file-pdf-o"></i> fa-file-pdf-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-file-photo-o"></i> fa-file-photo-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-file-picture-o"></i> fa-file-picture-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-file-powerpoint-o"></i> fa-file-powerpoint-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-file-sound-o"></i> fa-file-sound-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-file-video-o"></i> fa-file-video-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-file-word-o"></i> fa-file-word-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-file-zip-o"></i> fa-file-zip-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-film"></i> fa-film</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-filter"></i> fa-filter</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-fire"></i> fa-fire</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-fire-extinguisher"></i> fa-fire-extinguisher</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-flag"></i> fa-flag</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-flag-checkered"></i> fa-flag-checkered</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-flag-o"></i> fa-flag-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-flash"></i> fa-flash</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-flask"></i> fa-flask</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-folder"></i> fa-folder</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-folder-o"></i> fa-folder-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-folder-open"></i> fa-folder-open</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-folder-open-o"></i> fa-folder-open-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-frown-o"></i> fa-frown-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-futbol-o"></i> fa-futbol-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-gamepad"></i> fa-gamepad</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-gavel"></i> fa-gavel</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-gear"></i> fa-gear</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-gears"></i> fa-gears</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-gift"></i> fa-gift</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-glass"></i> fa-glass</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-globe"></i> fa-globe</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-graduation-cap"></i> fa-graduation-cap</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-group"></i> fa-group</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-hand-grab-o"></i> fa-hand-grab-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-hand-lizard-o"></i> fa-hand-lizard-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-hand-paper-o"></i> fa-hand-paper-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-hand-peace-o"></i> fa-hand-peace-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-hand-pointer-o"></i> fa-hand-pointer-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-hand-rock-o"></i> fa-hand-rock-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-hand-scissors-o"></i> fa-hand-scissors-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-hand-spock-o"></i> fa-hand-spock-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-hand-stop-o"></i> fa-hand-stop-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-handshake-o"></i> fa-handshake-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-hard-of-hearing"></i> fa-hard-of-hearing</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-hashtag"></i> fa-hashtag</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-hdd-o"></i> fa-hdd-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-headphones"></i> fa-headphones</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-heart"></i> fa-heart</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-heart-o"></i> fa-heart-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-heartbeat"></i> fa-heartbeat</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-history"></i> fa-history</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-home"></i> fa-home</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-hotel"></i> fa-hotel</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-hourglass"></i> fa-hourglass</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-hourglass-1"></i> fa-hourglass-1</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-hourglass-2"></i> fa-hourglass-2</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-hourglass-3"></i> fa-hourglass-3</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-hourglass-end"></i> fa-hourglass-end</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-hourglass-half"></i> fa-hourglass-half</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-hourglass-o"></i> fa-hourglass-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-hourglass-start"></i> fa-hourglass-start</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-i-cursor"></i> fa-i-cursor</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-id-badge"></i> fa-id-badge</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-id-card"></i> fa-id-card</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-id-card-o"></i> fa-id-card-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-image"></i> fa-image</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-inbox"></i> fa-inbox</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-industry"></i> fa-industry</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-info"></i> fa-info</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-info-circle"></i> fa-info-circle</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-institution"></i> fa-institution</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-key"></i> fa-key</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-keyboard-o"></i> fa-keyboard-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-language"></i> fa-language</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-laptop"></i> fa-laptop</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-leaf"></i> fa-leaf</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-legal"></i> fa-legal</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-lemon-o"></i> fa-lemon-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-level-down"></i> fa-level-down</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-level-up"></i> fa-level-up</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-life-bouy"></i> fa-life-bouy</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-life-buoy"></i> fa-life-buoy</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-life-ring"></i> fa-life-ring</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-life-saver"></i> fa-life-saver</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-lightbulb-o"></i> fa-lightbulb-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-line-chart"></i> fa-line-chart</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-location-arrow"></i> fa-location-arrow</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-lock"></i> fa-lock</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-low-vision"></i> fa-low-vision</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-magic"></i> fa-magic</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-magnet"></i> fa-magnet</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-mail-forward"></i> fa-mail-forward</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-mail-reply"></i> fa-mail-reply</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-mail-reply-all"></i> fa-mail-reply-all</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-male"></i> fa-male</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-map"></i> fa-map</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-map-marker"></i> fa-map-marker</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-map-o"></i> fa-map-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-map-pin"></i> fa-map-pin</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-map-signs"></i> fa-map-signs</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-meh-o"></i> fa-meh-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-microchip"></i> fa-microchip</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-microphone"></i> fa-microphone</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-microphone-slash"></i> fa-microphone-slash</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-minus"></i> fa-minus</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-minus-circle"></i> fa-minus-circle</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-minus-square"></i> fa-minus-square</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-minus-square-o"></i> fa-minus-square-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-mobile"></i> fa-mobile</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-mobile-phone"></i> fa-mobile-phone</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-money"></i> fa-money</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-moon-o"></i> fa-moon-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-mortar-board"></i> fa-mortar-board</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-motorcycle"></i> fa-motorcycle</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-mouse-pointer"></i> fa-mouse-pointer</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-music"></i> fa-music</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-navicon"></i> fa-navicon</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-newspaper-o"></i> fa-newspaper-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-object-group"></i> fa-object-group</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-object-ungroup"></i> fa-object-ungroup</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-paint-brush"></i> fa-paint-brush</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-paper-plane"></i> fa-paper-plane</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-paper-plane-o"></i> fa-paper-plane-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-paw"></i> fa-paw</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-pencil"></i> fa-pencil</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-pencil-square"></i> fa-pencil-square</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-pencil-square-o"></i> fa-pencil-square-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-percent"></i> fa-percent</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-phone"></i> fa-phone</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-phone-square"></i> fa-phone-square</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-photo"></i> fa-photo</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-picture-o"></i> fa-picture-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-pie-chart"></i> fa-pie-chart</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-plane"></i> fa-plane</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-plug"></i> fa-plug</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-plus"></i> fa-plus</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-plus-circle"></i> fa-plus-circle</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-plus-square"></i> fa-plus-square</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-plus-square-o"></i> fa-plus-square-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-podcast"></i> fa-podcast</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-power-off"></i> fa-power-off</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-print"></i> fa-print</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-puzzle-piece"></i> fa-puzzle-piece</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-qrcode"></i> fa-qrcode</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-question"></i> fa-question</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-question-circle"></i> fa-question-circle</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-question-circle-o"></i> fa-question-circle-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-quote-left"></i> fa-quote-left</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-quote-right"></i> fa-quote-right</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-random"></i> fa-random</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-recycle"></i> fa-recycle</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-refresh"></i> fa-refresh</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-registered"></i> fa-registered</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-remove"></i> fa-remove</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-reorder"></i> fa-reorder</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-reply"></i> fa-reply</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-reply-all"></i> fa-reply-all</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-retweet"></i> fa-retweet</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-road"></i> fa-road</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-rocket"></i> fa-rocket</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-rss"></i> fa-rss</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-rss-square"></i> fa-rss-square</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-s15"></i> fa-s15</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-search"></i> fa-search</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-search-minus"></i> fa-search-minus</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-search-plus"></i> fa-search-plus</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-send"></i> fa-send</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-send-o"></i> fa-send-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-server"></i> fa-server</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-share"></i> fa-share</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-share-alt"></i> fa-share-alt</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-share-alt-square"></i> fa-share-alt-square</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-share-square"></i> fa-share-square</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-share-square-o"></i> fa-share-square-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-shield"></i> fa-shield</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-ship"></i> fa-ship</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-shopping-bag"></i> fa-shopping-bag</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-shopping-basket"></i> fa-shopping-basket</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-shopping-cart"></i> fa-shopping-cart</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-shower"></i> fa-shower</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-sign-in"></i> fa-sign-in</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-sign-language"></i> fa-sign-language</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-sign-out"></i> fa-sign-out</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-signal"></i> fa-signal</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-signing"></i> fa-signing</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-sitemap"></i> fa-sitemap</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-sliders"></i> fa-sliders</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-smile-o"></i> fa-smile-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-snowflake-o"></i> fa-snowflake-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-soccer-ball-o"></i> fa-soccer-ball-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-sort"></i> fa-sort</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-sort-alpha-asc"></i> fa-sort-alpha-asc</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-sort-alpha-desc"></i> fa-sort-alpha-desc</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-sort-amount-asc"></i> fa-sort-amount-asc</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-sort-amount-desc"></i> fa-sort-amount-desc</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-sort-asc"></i> fa-sort-asc</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-sort-desc"></i> fa-sort-desc</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-sort-down"></i> fa-sort-down</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-sort-numeric-asc"></i> fa-sort-numeric-asc</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-sort-numeric-desc"></i> fa-sort-numeric-desc</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-sort-up"></i> fa-sort-up</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-space-shuttle"></i> fa-space-shuttle</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-spinner"></i> fa-spinner</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-spoon"></i> fa-spoon</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-square"></i> fa-square</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-square-o"></i> fa-square-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-star"></i> fa-star</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-star-half"></i> fa-star-half</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-star-half-empty"></i> fa-star-half-empty</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-star-half-full"></i> fa-star-half-full</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-star-half-o"></i> fa-star-half-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-star-o"></i> fa-star-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-sticky-note"></i> fa-sticky-note</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-sticky-note-o"></i> fa-sticky-note-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-street-view"></i> fa-street-view</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-suitcase"></i> fa-suitcase</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-sun-o"></i> fa-sun-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-support"></i> fa-support</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-tablet"></i> fa-tablet</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-tachometer"></i> fa-tachometer</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-tag"></i> fa-tag</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-tags"></i> fa-tags</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-tasks"></i> fa-tasks</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-taxi"></i> fa-taxi</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-television"></i> fa-television</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-terminal"></i> fa-terminal</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-thermometer"></i> fa-thermometer</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-thermometer-0"></i> fa-thermometer-0</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-thermometer-1"></i> fa-thermometer-1</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-thermometer-2"></i> fa-thermometer-2</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-thermometer-3"></i> fa-thermometer-3</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-thermometer-4"></i> fa-thermometer-4</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-thermometer-empty"></i> fa-thermometer-empty</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-thermometer-full"></i> fa-thermometer-full</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-thermometer-half"></i> fa-thermometer-half</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-thermometer-quarter"></i> fa-thermometer-quarter</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-thermometer-three-quarters"></i> fa-thermometer-three-quarters</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-thumb-tack"></i> fa-thumb-tack</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-thumbs-down"></i> fa-thumbs-down</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-thumbs-o-down"></i> fa-thumbs-o-down</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-thumbs-o-up"></i> fa-thumbs-o-up</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-thumbs-up"></i> fa-thumbs-up</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-ticket"></i> fa-ticket</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-times"></i> fa-times</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-times-circle"></i> fa-times-circle</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-times-circle-o"></i> fa-times-circle-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-times-rectangle"></i> fa-times-rectangle</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-times-rectangle-o"></i> fa-times-rectangle-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-tint"></i> fa-tint</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-toggle-down"></i> fa-toggle-down</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-toggle-left"></i> fa-toggle-left</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-toggle-off"></i> fa-toggle-off</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-toggle-on"></i> fa-toggle-on</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-toggle-right"></i> fa-toggle-right</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-toggle-up"></i> fa-toggle-up</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-trademark"></i> fa-trademark</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-trash"></i> fa-trash</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-trash-o"></i> fa-trash-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-tree"></i> fa-tree</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-trophy"></i> fa-trophy</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-truck"></i> fa-truck</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-tty"></i> fa-tty</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-tv"></i> fa-tv</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-umbrella"></i> fa-umbrella</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-universal-access"></i> fa-universal-access</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-university"></i> fa-university</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-unlock"></i> fa-unlock</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-unlock-alt"></i> fa-unlock-alt</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-unsorted"></i> fa-unsorted</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-upload"></i> fa-upload</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-user"></i> fa-user</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-user-circle"></i> fa-user-circle</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-user-circle-o"></i> fa-user-circle-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-user-o"></i> fa-user-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-user-plus"></i> fa-user-plus</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-user-secret"></i> fa-user-secret</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-user-times"></i> fa-user-times</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-users"></i> fa-users</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-vcard"></i> fa-vcard</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-vcard-o"></i> fa-vcard-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-video-camera"></i> fa-video-camera</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-volume-control-phone"></i> fa-volume-control-phone</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-volume-down"></i> fa-volume-down</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-volume-off"></i> fa-volume-off</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-volume-up"></i> fa-volume-up</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-warning"></i> fa-warning</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-wheelchair"></i> fa-wheelchair</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-wheelchair-alt"></i> fa-wheelchair-alt</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-wifi"></i> fa-wifi</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-window-close"></i> fa-window-close</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-window-close-o"></i> fa-window-close-o</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-window-maximize"></i> fa-window-maximize</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-window-minimize"></i> fa-window-minimize</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-window-restore"></i> fa-window-restore</div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="fa fa-wrench"></i> fa-wrench</div>

                </div>
            </div>
        </div>
        <!-- End .widget -->


        <div class="box box-info">
            <!-- Start .widget -->
            <div class="box-header with-border">
                <h3 class="box-title">DMIcon</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-box="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-emo-happy"></i>icon-emo-happy</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-emo-wink"></i>icon-emo-wink</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-emo-unhappy"></i>icon-emo-unhappy</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-emo-sleep"></i>icon-emo-sleep</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-emo-thumbsup"></i>icon-emo-thumbsup</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-emo-devil"></i>icon-emo-devil</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-emo-surprised"></i>icon-emo-surprised</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-emo-tongue"></i>icon-emo-tongue</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-emo-coffee"></i>icon-emo-coffee</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-emo-sunglasses"></i>icon-emo-sunglasses</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-emo-displeased"></i>icon-emo-displeased</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-emo-beer"></i>icon-emo-beer</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-emo-grin"></i>icon-emo-grin</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-emo-angry"></i>icon-emo-angry</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-emo-saint"></i>icon-emo-saint</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-emo-cry"></i>icon-emo-cry</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-emo-shoot"></i>icon-emo-shoot</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-emo-squint"></i>icon-emo-squint</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-emo-laugh"></i>icon-emo-laugh</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-emo-wink2"></i>icon-emo-wink2</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-mail"></i>icon-mail</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-search"></i>icon-search</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-heart"></i>icon-heart</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-heart-empty"></i>icon-heart-empty</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-star"></i>icon-star</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-star-empty"></i>icon-star-empty</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-star-half"></i>icon-star-half</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-user"></i>icon-user</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-users"></i>icon-users</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-th-large"></i>icon-th-large</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-th"></i>icon-th</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-th-list"></i>icon-th-list</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-ok"></i>icon-ok</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-ok-circled"></i>icon-ok-circled</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-ok-circled2"></i>icon-ok-circled2</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-cancel"></i>icon-cancel</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-cancel-circled"></i>icon-cancel-circled</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-cancel-circled2"></i>icon-cancel-circled2</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-plus"></i>icon-plus</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-plus-circled"></i>icon-plus-circled</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-minus"></i>icon-minus</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-minus-circled"></i>icon-minus-circled</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-help-circled"></i>icon-help-circled</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-info-circled"></i>icon-info-circled</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-home"></i>icon-home</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-attach"></i>icon-attach</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-lock"></i>icon-lock</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-lock-open"></i>icon-lock-open</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-spin1 animate-spin"></i>icon-spin1 animate-spin</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-spin2 animate-spin"></i>icon-spin2 animate-spin</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-spin3 animate-spin"></i>icon-spin3 animate-spin</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-pin"></i>icon-pin</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-spin4 animate-spin"></i>icon-spin4 animate-spin</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-eye"></i>icon-eye</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-eye-off"></i>icon-eye-off</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-tag"></i>icon-tag</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-spin5 animate-spin"></i>icon-spin5 animate-spin</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-spin6 animate-spin"></i>icon-spin6 animate-spin</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-tags"></i>icon-tags</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-bookmark"></i>icon-bookmark</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-flag"></i>icon-flag</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-thumbs-up"></i>icon-thumbs-up</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-thumbs-down"></i>icon-thumbs-down</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-download"></i>icon-download</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-firefox"></i>icon-firefox</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-chrome"></i>icon-chrome</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-opera"></i>icon-opera</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-ie"></i>icon-ie</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-crown"></i>icon-crown</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-crown-plus"></i>icon-crown-plus</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-crown-minus"></i>icon-crown-minus</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-marquee"></i>icon-marquee</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-upload"></i>icon-upload</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-forward"></i>icon-forward</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-export"></i>icon-export</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-pencil"></i>icon-pencil</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-edit"></i>icon-edit</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-print"></i>icon-print</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-retweet"></i>icon-retweet</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-comment"></i>icon-comment</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-chat"></i>icon-chat</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-bell"></i>icon-bell</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-attention"></i>icon-attention</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-attention-circled"></i>icon-attention-circled</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-location"></i>icon-location</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-trash-empty"></i>icon-trash-empty</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-doc"></i>icon-doc</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-folder"></i>icon-folder</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-folder-open"></i>icon-folder-open</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-phone"></i>icon-phone</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-cog"></i>icon-cog</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-cog-alt"></i>icon-cog-alt</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-wrench"></i>icon-wrench</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-basket"></i>icon-basket</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-calendar"></i>icon-calendar</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-login"></i>icon-login</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-logout"></i>icon-logout</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-volume-off"></i>icon-volume-off</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-volume-down"></i>icon-volume-down</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-volume-up"></i>icon-volume-up</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-headphones"></i>icon-headphones</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-clock"></i>icon-clock</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-block"></i>icon-block</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-resize-full"></i>icon-resize-full</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-resize-small"></i>icon-resize-small</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-resize-vertical"></i>icon-resize-vertical</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-resize-horizontal"></i>icon-resize-horizontal</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-zoom-in"></i>icon-zoom-in</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-zoom-out"></i>icon-zoom-out</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-down-circled2"></i>icon-down-circled2</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-up-circled2"></i>icon-up-circled2</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-down-dir"></i>icon-down-dir</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-up-dir"></i>icon-up-dir</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-left-dir"></i>icon-left-dir</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-right-dir"></i>icon-right-dir</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-down-open"></i>icon-down-open</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-left-open"></i>icon-left-open</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-right-open"></i>icon-right-open</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-up-open"></i>icon-up-open</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-down-big"></i>icon-down-big</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-left-big"></i>icon-left-big</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-right-big"></i>icon-right-big</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-up-big"></i>icon-up-big</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-right-hand"></i>icon-right-hand</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-left-hand"></i>icon-left-hand</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-up-hand"></i>icon-up-hand</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-down-hand"></i>icon-down-hand</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-cw"></i>icon-cw</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-ccw"></i>icon-ccw</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-arrows-cw"></i>icon-arrows-cw</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-shuffle"></i>icon-shuffle</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-play"></i>icon-play</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-play-circled2"></i>icon-play-circled2</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-stop"></i>icon-stop</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-pause"></i>icon-pause</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-to-end"></i>icon-to-end</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-to-end-alt"></i>icon-to-end-alt</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-to-start"></i>icon-to-start</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-to-start-alt"></i>icon-to-start-alt</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-fast-fw"></i>icon-fast-fw</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-fast-bw"></i>icon-fast-bw</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-eject"></i>icon-eject</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-target"></i>icon-target</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-signal"></i>icon-signal</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-award"></i>icon-award</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-inbox"></i>icon-inbox</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-globe"></i>icon-globe</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-cloud"></i>icon-cloud</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-flash"></i>icon-flash</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-scissors"></i>icon-scissors</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-briefcase"></i>icon-briefcase</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-italic"></i>icon-italic</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-text-height"></i>icon-text-height</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-text-width"></i>icon-text-width</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-align-left"></i>icon-align-left</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-align-center"></i>icon-align-center</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-align-right"></i>icon-align-right</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-align-justify"></i>icon-align-justify</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-list"></i>icon-list</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-indent-left"></i>icon-indent-left</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-indent-right"></i>icon-indent-right</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-off"></i>icon-off</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-road"></i>icon-road</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-list-alt"></i>icon-list-alt</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-qrcode"></i>icon-qrcode</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-barcode"></i>icon-barcode</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-book"></i>icon-book</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-adjust"></i>icon-adjust</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-tint"></i>icon-tint</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-check"></i>icon-check</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-asterisk"></i>icon-asterisk</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-gift"></i>icon-gift</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-fire"></i>icon-fire</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-magnet"></i>icon-magnet</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-chart-bar"></i>icon-chart-bar</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-credit-card"></i>icon-credit-card</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-floppy"></i>icon-floppy</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-megaphone"></i>icon-megaphone</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-key"></i>icon-key</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-hammer"></i>icon-hammer</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-login-1"></i>icon-login-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-logout-1"></i>icon-logout-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-cup"></i>icon-cup</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-trash-1"></i>icon-trash-1</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-bell-1"></i>icon-bell-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-attention-1"></i>icon-attention-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-alert"></i>icon-alert</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-chat-1"></i>icon-chat-1</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-print-1"></i>icon-print-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-keyboard-1"></i>icon-keyboard-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-comment-1"></i>icon-comment-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-tag-1"></i>icon-tag-1</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-check-1"></i>icon-check-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-cancel-1"></i>icon-cancel-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-cancel-circled-1"></i>icon-cancel-circled-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-cancel-squared"></i>icon-cancel-squared</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-plus-1"></i>icon-plus-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-plus-circled-1"></i>icon-plus-circled-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-plus-squared-1"></i>icon-plus-squared-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-minus-1"></i>icon-minus-1</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-minus-circled-1"></i>icon-minus-circled-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-minus-squared-1"></i>icon-minus-squared-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-help-1"></i>icon-help-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-help-circled-1"></i>icon-help-circled-1</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-info-1"></i>icon-info-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-info-circled-1"></i>icon-info-circled-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-home-1"></i>icon-home-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-user-1"></i>icon-user-1</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-users-1"></i>icon-users-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-user-add"></i>icon-user-add</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-video"></i>icon-video</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-picture"></i>icon-picture</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-lock-1"></i>icon-lock-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-lock-open-1"></i>icon-lock-open-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-layout"></i>icon-layout</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-menu-1"></i>icon-menu-1</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-attach-1"></i>icon-attach-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-docs-1"></i>icon-docs-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-book-1"></i>icon-book-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-folder-1"></i>icon-folder-1</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-archive"></i>icon-archive</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-box-1"></i>icon-box-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-left-open-mini"></i>icon-left-open-mini</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-right-open-mini"></i>icon-right-open-mini</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-up-open-mini"></i>icon-up-open-mini</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-down-open-big"></i>icon-down-open-big</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-left-open-big"></i>icon-left-open-big</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-right-open-big"></i>icon-right-open-big</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-up-open-big"></i>icon-up-open-big</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-down-open-1"></i>icon-down-open-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-left-open-1"></i>icon-left-open-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-right-open-1"></i>icon-right-open-1</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-up-open-1"></i>icon-up-open-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-down-open-mini"></i>icon-down-open-mini</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-down-circled-1"></i>icon-down-circled-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-left-circled-1"></i>icon-left-circled-1</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-right-circled-1"></i>icon-right-circled-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-up-circled-1"></i>icon-up-circled-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-popup"></i>icon-popup</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-resize-small-1"></i>icon-resize-small-1</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-resize-full-1"></i>icon-resize-full-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-down-1"></i>icon-down-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-left-1"></i>icon-left-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-right-1"></i>icon-right-1</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-up-1"></i>icon-up-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-down-dir-1"></i>icon-down-dir-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-left-dir-1"></i>icon-left-dir-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-right-dir-1"></i>icon-right-dir-1</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-up-dir-1"></i>icon-up-dir-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-down-bold"></i>icon-down-bold</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-left-bold"></i>icon-left-bold</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-right-bold"></i>icon-right-bold</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-up-bold"></i>icon-up-bold</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-down-thin"></i>icon-down-thin</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-left-thin"></i>icon-left-thin</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-right-thin"></i>icon-right-thin</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-up-thin"></i>icon-up-thin</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-ccw-1"></i>icon-ccw-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-cw-1"></i>icon-cw-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-arrows-ccw"></i>icon-arrows-ccw</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-level-down-1"></i>icon-level-down-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-level-up-1"></i>icon-level-up-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-shuffle-1"></i>icon-shuffle-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-cloud-1"></i>icon-cloud-1</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-cloud-thunder"></i>icon-cloud-thunder</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-target-1"></i>icon-target-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-mobile-1"></i>icon-mobile-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-cd"></i>icon-cd</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-inbox-1"></i>icon-inbox-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-globe-1"></i>icon-globe-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-cloud-2"></i>icon-cloud-2</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-paper-plane"></i>icon-paper-plane</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-fire-1"></i>icon-fire-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-params"></i>icon-params</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-calendar-1"></i>icon-calendar-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-sound"></i>icon-sound</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-clock-1"></i>icon-clock-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-lightbulb-1"></i>icon-lightbulb-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-tv"></i>icon-tv</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-music"></i>icon-music</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-search-1"></i>icon-search-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-mail-1"></i>icon-mail-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-heart-1"></i>icon-heart-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-star-1"></i>icon-star-1</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-user-2"></i>icon-user-2</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-videocam"></i>icon-videocam</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-camera"></i>icon-camera</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-photo"></i>icon-photo</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-attach-2"></i>icon-attach-2</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-lock-2"></i>icon-lock-2</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-eye-1"></i>icon-eye-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-tag-2"></i>icon-tag-2</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-thumbs-up-1"></i>icon-thumbs-up-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-pencil-1"></i>icon-pencil-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-comment-2"></i>icon-comment-2</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-location-1"></i>icon-location-1</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-cup-1"></i>icon-cup-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-trash-2"></i>icon-trash-2</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-doc-1"></i>icon-doc-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-note"></i>icon-note</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-cog-1"></i>icon-cog-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-graduation-cap"></i>icon-graduation-cap</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-megaphone-1"></i>icon-megaphone-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-database-1"></i>icon-database-1</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-key-1"></i>icon-key-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-beaker"></i>icon-beaker</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-truck"></i>icon-truck</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-desktop-1"></i>icon-desktop-1</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-money"></i>icon-money</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-food-1"></i>icon-food-1</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-shop"></i>icon-shop</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-diamond"></i>icon-diamond</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-t-shirt"></i>icon-t-shirt</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-wallet"></i>icon-wallet</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-move"></i>icon-move</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-link-ext"></i>icon-link-ext</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-check-empty"></i>icon-check-empty</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-bookmark-empty"></i>icon-bookmark-empty</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-phone-squared"></i>icon-phone-squared</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-rss"></i>icon-rss</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-hdd"></i>icon-hdd</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-certificate"></i>icon-certificate</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-left-circled"></i>icon-left-circled</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-right-circled"></i>icon-right-circled</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-up-circled"></i>icon-up-circled</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-down-circled"></i>icon-down-circled</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-tasks"></i>icon-tasks</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-filter"></i>icon-filter</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-resize-full-alt"></i>icon-resize-full-alt</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-docs"></i>icon-docs</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-menu"></i>icon-menu</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-list-bullet"></i>icon-list-bullet</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-list-numbered"></i>icon-list-numbered</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-strike"></i>icon-strike</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-underline"></i>icon-underline</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-table"></i>icon-table</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-magic"></i>icon-magic</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-columns"></i>icon-columns</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-sort"></i>icon-sort</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-sort-down"></i>icon-sort-down</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-sort-up"></i>icon-sort-up</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-mail-alt"></i>icon-mail-alt</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-gauge"></i>icon-gauge</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-comment-empty"></i>icon-comment-empty</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-chat-empty"></i>icon-chat-empty</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-sitemap"></i>icon-sitemap</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-paste"></i>icon-paste</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-lightbulb"></i>icon-lightbulb</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-exchange"></i>icon-exchange</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-download-cloud"></i>icon-download-cloud</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-upload-cloud"></i>icon-upload-cloud</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-suitcase"></i>icon-suitcase</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-bell-alt"></i>icon-bell-alt</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-coffee"></i>icon-coffee</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-food"></i>icon-food</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-doc-text"></i>icon-doc-text</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-beer"></i>icon-beer</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-plus-squared"></i>icon-plus-squared</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-angle-double-left"></i>icon-angle-double-left</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-angle-double-right"></i>icon-angle-double-right</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-angle-double-up"></i>icon-angle-double-up</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-angle-double-down"></i>icon-angle-double-down</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-angle-left"></i>icon-angle-left</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-angle-right"></i>icon-angle-right</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-angle-up"></i>icon-angle-up</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-angle-down"></i>icon-angle-down</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-desktop"></i>icon-desktop</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-laptop"></i>icon-laptop</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-tablet"></i>icon-tablet</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-mobile"></i>icon-mobile</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-circle-empty"></i>icon-circle-empty</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-quote-left"></i>icon-quote-left</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-quote-right"></i>icon-quote-right</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-spinner"></i>icon-spinner</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-circle"></i>icon-circle</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-reply"></i>icon-reply</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-folder-empty"></i>icon-folder-empty</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-folder-open-empty"></i>icon-folder-open-empty</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-smile"></i>icon-smile</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-frown"></i>icon-frown</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-meh"></i>icon-meh</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-gamepad"></i>icon-gamepad</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-keyboard"></i>icon-keyboard</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-flag-empty"></i>icon-flag-empty</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-flag-checkered"></i>icon-flag-checkered</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-code"></i>icon-code</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-reply-all"></i>icon-reply-all</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-star-half-alt"></i>icon-star-half-alt</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-direction"></i>icon-direction</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-crop"></i>icon-crop</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-fork"></i>icon-fork</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-help"></i>icon-help</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-info"></i>icon-info</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-attention-alt"></i>icon-attention-alt</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-superscript"></i>icon-superscript</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-subscript"></i>icon-subscript</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-mic"></i>icon-mic</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-mute"></i>icon-mute</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-calendar-empty"></i>icon-calendar-empty</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-angle-circled-left"></i>icon-angle-circled-left</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-angle-circled-right"></i>icon-angle-circled-right</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-angle-circled-up"></i>icon-angle-circled-up</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-angle-circled-down"></i>icon-angle-circled-down</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-lock-open-alt"></i>icon-lock-open-alt</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-ellipsis"></i>icon-ellipsis</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-ellipsis-vert"></i>icon-ellipsis-vert</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-rss-squared"></i>icon-rss-squared</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-play-circled"></i>icon-play-circled</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-minus-squared"></i>icon-minus-squared</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-minus-squared-alt"></i>icon-minus-squared-alt</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-level-up"></i>icon-level-up</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-level-down"></i>icon-level-down</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-ok-squared"></i>icon-ok-squared</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-pencil-squared"></i>icon-pencil-squared</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-link-ext-alt"></i>icon-link-ext-alt</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-export-alt"></i>icon-export-alt</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-compass"></i>icon-compass</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-expand"></i>icon-expand</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-collapse"></i>icon-collapse</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-expand-right"></i>icon-expand-right</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-doc-inv"></i>icon-doc-inv</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-doc-text-inv"></i>icon-doc-text-inv</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-sort-name-up"></i>icon-sort-name-up</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-sort-name-down"></i>icon-sort-name-down</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-sort-alt-up"></i>icon-sort-alt-up</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-sort-alt-down"></i>icon-sort-alt-down</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-sort-number-up"></i>icon-sort-number-up</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-sort-number-down"></i>icon-sort-number-down</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-thumbs-up-alt"></i>icon-thumbs-up-alt</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-thumbs-down-alt"></i>icon-thumbs-down-alt</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-down"></i>icon-down</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-up"></i>icon-up</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-left"></i>icon-left</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-right"></i>icon-right</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-female"></i>icon-female</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-male"></i>icon-male</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-sun"></i>icon-sun</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-moon"></i>icon-moon</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-box"></i>icon-box</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-bug"></i>icon-bug</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-right-circled2"></i>icon-right-circled2</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-left-circled2"></i>icon-left-circled2</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-collapse-left"></i>icon-collapse-left</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-dot-circled"></i>icon-dot-circled</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-plus-squared-alt"></i>icon-plus-squared-alt</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-fax"></i>icon-fax</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-child"></i>icon-child</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-cube"></i>icon-cube</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-cubes"></i>icon-cubes</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-database"></i>icon-database</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-file-pdf"></i>icon-file-pdf</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-file-word"></i>icon-file-word</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-file-excel"></i>icon-file-excel</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-file-powerpoint"></i>icon-file-powerpoint</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-file-image"></i>icon-file-image</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-file-archive"></i>icon-file-archive</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-file-audio"></i>icon-file-audio</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-file-video"></i>icon-file-video</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-file-code"></i>icon-file-code</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-circle-notch"></i>icon-circle-notch</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-history"></i>icon-history</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-circle-thin"></i>icon-circle-thin</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-header"></i>icon-header</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-paragraph"></i>icon-paragraph</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-sliders"></i>icon-sliders</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-share"></i>icon-share</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-share-squared"></i>icon-share-squared</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-wifi"></i>icon-wifi</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-bell-off"></i>icon-bell-off</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-bell-off-empty"></i>icon-bell-off-empty</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-trash"></i>icon-trash</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-chart-area"></i>icon-chart-area</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-chart-pie"></i>icon-chart-pie</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-chart-line"></i>icon-chart-line</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-toggle-off"></i>icon-toggle-off</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-toggle-on"></i>icon-toggle-on</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-cart-plus"></i>icon-cart-plus</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-cart-arrow-down"></i>icon-cart-arrow-down</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-user-secret"></i>icon-user-secret</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-server"></i>icon-server</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-user-plus"></i>icon-user-plus</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-user-times"></i>icon-user-times</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-calendar-plus-o"></i>icon-calendar-plus-o</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-calendar-minus-o"></i>icon-calendar-minus-o</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-calendar-times-o"></i>icon-calendar-times-o</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-calendar-check-o"></i>icon-calendar-check-o</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-commenting-o"></i>icon-commenting-o</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-address-book"></i>icon-address-book</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-address-book-o"></i>icon-address-book-o</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-address-card"></i>icon-address-card</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-address-card-o"></i>icon-address-card-o</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-user-circle"></i>icon-user-circle</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-user-circle-o"></i>icon-user-circle-o</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-user-o"></i>icon-user-o</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-id-badge"></i>icon-id-badge</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-id-card"></i>icon-id-card</div>


                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><i class="icon-id-card-o"></i>icon-id-card-o</div>

                </div>
            </div>
        </div>
    </div>
    <!-- End .row -->


    <script src="<?= $assets?>/js/jquery.quicksearch.js"></script>

    <script type="text/javascript">
        $(function () {

            $(".box").box('addBtn');

            if ($('input[name=searchIcon]').length) {
                $('input[name=searchIcon]').val('').quicksearch('.col-lg-3', {
                    'removeDiacritics': true
                });
            }
        });
    </script>
