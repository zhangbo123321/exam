<div class="box box-success" id="category" style="height: calc(100% - 20px) ;">
    <div class="box-header with-border">
        <h3 class="box-title">
            类别管理
        </h3>
        <div class="pull-right box-tools">
            <button class="btn btn-box-tool" id="collapsecategory" data-collapse=false type="button">
                <i class="fa fa-sort-up"></i>
            </button>
            <button class="btn btn-box-tool addcategory" id="addBtn" type="button">
                <i class="fa fa-plus"></i>
            </button>
        </div>
    </div>
    <div class="box-body" style="overflow-y: auto;height:calc(100% - 55px);">
        <div class="dd" id="nestable-json">
        </div>
    </div>
</div>

<script src="<?= $assets ?>/js/jquery.nestable.min.js" type="text/javascript">
</script>
<script type="text/javascript">
    $(function () {
        var obj = '<?=$categories?>';
        $('#nestable-json').nestable({
            'json': obj,
            tool: "<div class='pull-right box-tools' style='position:absolute;top:5px;right:2px;'>"
            + '<button type="button" class="btn btn-box-tool addcategory"><i class="fa fa-plus"></i></button>'
            + '<button type="button" class="btn btn-box-tool editcategory"><i class="fa fa-pencil"></i></button>'
            + '<button type="button" class="btn btn-box-tool delcategory"><i class="fa fa-remove"></i></button>'
            + '</div>',
            contentCallback: function (item) {
                var name = item.name || '' ? item.name : item.id;
                return name;
            },
            callback: function (l, e, p) {
                var id = e.data('id');
                var parent = e.parents('li').data('id');
                parent = parent != null ? parent : 0;

                if (e.data('parent') == parent) return;
                e.data('parent', parent);
                $.post("<?=site_url('admin/category/parent')?>", {'id': id, 'parent': parent}, function () {
                });
            }
        });

        $('#category').popover({
            html: true,
            placement: 'bottom',
            container: '#category',
            selector: '.addcategory',
            viewport: {selector: 'body', padding: 0},
            title: "新增类别",
            content: function () {
                var id = $(this).parents("li").data('id');
                id = id ? id : 0;
                var c = '<div class="input-group" style="width: 200px;">' +
                    '<input type="hidden" name="parentID" value="' + id + '"/>' +
                    '<input class="form-control" name="categoryName" placeholder="Name" type="text"/>' +
                    '<span class="input-group-btn">' +
                    '<button class="btn btn-primary savecategory" type="button">' +
                    '<i class="fa fa-check"></i>' +
                    '</button>' +
                    '<button class="btn btn-default cancleSave" type="button">' +
                    '<i class="fa fa-remove"></i>' +
                    '</button>' +
                    '</span>    </div>';
                return c;
            }
        });

        $(document).on('click', '.editcategory', function () {
            var content = $(this).parents('li').first().children('.dd3-content').first();
            var id = $(this).parents("li").data('id');
            if (content.data('edit') && content.data('edit') != "") {
                content.html(content.data('edit'));
                content.data('edit', "");
            } else {
                var str = '<div class="input-group input-group-sm" style="width: 200px;">' +
                    '<input name="id" type="hidden" value="' + id + '"/>' +
                    '<input class="form-control" name="categoryName" placeholder="Name" type="text" value="' + content.text() + '"/>' +
                    '<span class="input-group-btn">' +
                    '<button class="btn btn-primary saveEdit" type="button">' +
                    '<i class="fa fa-check"></i>' +
                    '</button>' +
                    '<button class="btn btn-default cancleEdit" type="button">' +
                    '<i class="fa fa-remove"></i>' +
                    '</button>' +
                    '</span>    </div>';
                content.data('edit', content.text());
                content.html(str);
            }
        });

        $(document).on('click', '.saveEdit', function () {
            var content = $(this).parents('.dd3-content').first();
            var id = $(this).parents('div.input-group').children('input[name=id]').val();
            var name = $(this).parents('div.input-group').children('input[name=categoryName]').val();
            $.post('<?=site_url("admin/category/name")?>', {'id': id, 'name': name}, function () {
                content.html(name);
                content.data('edit', "");
            });
        });

        $(document).on('click', '.cancleEdit', function () {
            $(this).parents('li').first().find('.editcategory').first().click();
        });


        $('#category').on('inserted.bs.popover', function () {
            $('input[name=categoryName]').focus();
        });

        $(document).on('blur', 'input[name=categoryName]', function () {
            $(this).parents(".popover").first().popover('destroy');
        });

        $(document).on('click', '.savecategory', function () {
            var parent = $(this).parents("div.input-group").children('input[name=parentID]').val();
            var name = $(this).parents("div.input-group").children('input[name=categoryName]').val();
            $.post("<?=site_url('admin/category/save')?>", {'parent': parent, 'name': name}, function (data) {
                $('#nestable-json').nestable('add', $.parseJSON(data));
            });
        });

        $("#collapsecategory").click(function() {
            var collapse=$(this).data('collapse');
            if (collapse) {
                $(this).data('collapse',false);
                $(this).children('i').removeClass("fa-sort-down").addClass('fa-sort-up');
                $('#nestable-json').nestable('expandAll');
            } else {
                $(this).data('collapse',true);
                $(this).children('i').removeClass("fa-sort-up").addClass('fa-sort-down');
                $('#nestable-json').nestable('collapseAll');
            }
        });

        $("#collapsecategory").click();

        $(document).on('click','.delcategory',function() {
            var id = $(this).parents('li:first').data('id');
            var name = $(this).parents('li:first').children('.dd3-content').text();

            BootstrapDialog.confirm({
                title: 'WARNING',
                message: "删除区域 <b>"+name+"</b>,将同时删除下级区域<br/>确认要删除此区域？",
                type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                btnCancelLabel: '取消',
                btnOKLabel: '删除!',
                btnOKClass: 'btn-warning',
                callback: function(result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if(result) {
                        $.get('<?=site_url("/admin/category/delete")?>/'+id,function(data) {
                            showMessage(data.message);
                            if (data.status != "Error" ) {
                                $('#nestable-json').nestable('remove', id);
                            }
                        })
                    }else {
                        alert('Nope.');
                    }
                }
            });

        });
    });
</script>