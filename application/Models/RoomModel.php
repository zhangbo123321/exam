<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 18-9-29
 * Time: 上午8:37
 */

namespace App\Models;
use CodeIgniter\Model;

class RoomModel extends Model {
    protected $table      = 'room';  //表名
    protected $primaryKey = 'id';  //索引

    protected $returnType     = 'array';  //返回类型
    protected $useSoftDeletes = false; //使用软删除？表内要有deleted列

    protected $allowedFields = ['name', 'page','time','start','zone','organize','stat'];  //允许更新写入的列

    protected $useTimestamps = false;  //使用时间戳

    protected $validationRules    = [];  //验证规则
    protected $validationMessages = [];  //验证返回信息
    protected $skipValidation     = false;  //在所有插入和更新期间，应跳过其他验证

    public function findUserRoom($zone,$organize,$select="*",$stat='0') {
        if ($stat=='0') {
            $this->where('time >',date('Y-m-d H:i:s',time()));
        }

        if ($organize) {
            $organize=is_array($organize)?implode(",",$organize):$organize;
            $this ->groupStart()
                  ->where('organize',null)
                  ->orWhere('organize REGEXP',"[[:<:]]{$organize}[[:>:]]")
                  ->groupEnd();
        } else {
            $this->where('organize',null);
        }

        return $this->select($select)
            ->whereIn('zone',$zone)
            ->where('stat',$stat)
            ->findAll();
    }
}