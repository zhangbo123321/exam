<?php namespace App\Models;

use CodeIgniter\Model;

class CategoryModel extends Model {
    protected $table      = 'category'; //表名
    protected $primaryKey = 'id'; //索引

    protected $returnType     = 'array'; //返回类型
    protected $useSoftDeletes = false; //使用软删除？表内要有deleted列

    protected $allowedFields = ['parent', 'name']; //允许更新写入的列

    protected $useTimestamps = false; //使用时间戳

    protected $validationRules    = []; //验证规则
    protected $validationMessages = []; //验证返回信息
    protected $skipValidation     = false; //在所有插入和更新期间，应跳过其他验证

    protected $children = [];
    protected $parents  = [];

    protected function buildTree($items, $parent = 0, $ckey = 'children') {
        $tree = array();
        foreach ($items as $rk => $rv) {
            if ($rv['parent'] == $parent) {
                $children = $this->buildTree($items, $rv['id']);
                if ($children) {
                    $rv[$ckey] = $children;
                }
                $tree[] = $rv;
            }
        }
        return $tree;
    }

    public function tree($id = 0) {
        $cities = $this->getChildren($id);
        return $this->buildTree($cities);
    }

    public function getChildren($id, $select = '*', $self = true) {
        $sql="select {$select} from ( select t1.id, if(find_in_set(parent, @pids) > 0, @pids := concat(@pids, ',', id), 0) as ischild from ( select id,parent from category t order by parent, id ) t1, (select @pids := {$id}) t2 ) t3 where ischild != 0 ";
        $sql.=$self?" OR id={$id}":'';

        $children = $this->db->query($sql)
            ->getResult('array');

        if (strtolower($select)=="id") {
            foreach($children as $c) {
                $child[]=$c['id'];
            }
        }

        return strtolower($select)=="id"?$child:$children;
    }

    public function getParents($id, $clear = true) {
        if ($clear) {
            $this->parents = [];
        }
        if ($id) {
            $city = $this->find($id);
            $this->parents[] = $city;
            $this->getParents($city['parent'], false);
        }
        return $this->parents;
    }

    public function deleteAll($id, $children = true) {
        $ids = $children ? $this->getChildren($id, 'id') : $id;
        var_dump($ids);
        $this->whereIn('id',$ids)->delete();
    }
}
