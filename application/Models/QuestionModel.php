<?php namespace App\Models;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 18-9-21
 * Time: 上午10:38
 */

use CodeIgniter\Model;

class QuestionModel extends Model {
    protected $table      = 'question'; //表名
    protected $primaryKey = 'id'; //索引

    protected $returnType     = 'array'; //返回类型
    protected $useSoftDeletes = true; //使用软删除？表内要有deleted列

    protected $allowedFields = ['title', 'category', 'type', 'option', 'level', 'analysis', 'date']; //允许更新写入的列

    protected $useTimestamps = false; //使用时间戳

    protected $validationRules    = []; //验证规则
    protected $validationMessages = []; //验证返回信息
    protected $skipValidation     = false; //在所有插入和更新期间，应跳过其他验证

    function emptyTrash() {
        $this->purgeDeleted();
    }

    public function recover($id) {
        $this->whereIn('id', $id)
             ->set('deleted', 0)
             ->update();
    }

    public function random($category, $type, $num) {
        return $this->select('id,title,option')
                    ->whereIn('category', $category)
                    ->where('type', $type)
                    ->orderBy('RAND()')
                    ->limit($num, 0)
                    ->find();
    }

    public function import($file) {
        $csv = new \ParseCsv\Csv();
        $csv->auto($file);

        var_dump($csv->data);
    }

}
