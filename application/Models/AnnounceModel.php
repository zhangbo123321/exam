<?php namespace App\Models;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 18-9-19
 * Time: 上午10:25
 */

use CodeIgniter\Model;

class AnnounceModel extends Model {
    protected $table      = 'announce';  //表名
    protected $primaryKey = 'id';  //索引

    protected $returnType     = 'array';  //返回类型
    protected $useSoftDeletes = false; //使用软删除？表内要有deleted列

    protected $allowedFields = ['title', 'content'];  //允许更新写入的列

    protected $useTimestamps = false;  //使用时间戳

    protected $validationRules    = [];  //验证规则
    protected $validationMessages = [];  //验证返回信息
    protected $skipValidation     = false;  //在所有插入和更新期间，应跳过其他验证

}
