<?php namespace App\Models;

use CodeIgniter\Model;

class GroupModel extends Model
{
    protected $table      = 'groups';  //表名
    protected $primaryKey = 'id';  //索引

    protected $returnType = 'array';  //返回类型
    protected $useSoftDeletes = false; //使用软删除？表内要有deleted列

    protected $allowedFields = ['level'];  //允许更新写入的列

    protected $useTimestamps = false;  //使用时间戳

    protected $validationRules    = [];  //验证规则
    protected $validationMessages = [];  //验证返回信息
    protected $skipValidation     = false;  //在所有插入和更新期间，应跳过其他验证

}
